package ejercicio04;

import java.util.Scanner;

public class ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		
		System.out.println("1.- Comprobar número");
		System.out.println("2.- Comprobar carácter");
		System.out.println("3.- Salir");
		
		
		int opcion=input.nextInt();
		
		if (opcion>=1 &&opcion <=2) {
			switch(opcion) {
			
			case 1:
				System.out.println("Introduce un número entero");
				int entero =input.nextInt();
				if (entero <0) {
					System.out.println("El número es negativo");
				}else if (entero %5 ==0) {
					System.out.println("Es múltiplo de 5");
				}else {
					System.out.println("No es múltiplo de 5");
				}
				break;
			case 2:
				//limpiar el buffer
				input.nextLine();
				System.out.println("Introduce una cadena");
				String cadena=input.nextLine();
				if (cadena.length()!=1) {
					System.out.println("Has introducido más ede un carácter");
					
				}else {
					
					int caracterAscii = (int)cadena.charAt(0);
					if ((caracterAscii>=65 && caracterAscii <=90) || (caracterAscii>=97 && caracterAscii<=122)) {
						
						System.out.println("Es una letra");
					} else if (caracterAscii >=48 && caracterAscii <=57) {
						System.out.println("Es un número");
					} else {
						System.out.println("Es un signo");
					}
				}
				
				break;
			}
			
		}else {
			
			System.out.println("La opción elegida no es correcta");
			System.out.println("Programa terminado");
		}
		
		input.close();
	}

}
