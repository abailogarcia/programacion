package ejercicio02;

import java.util.Scanner;

public class ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entrada = new Scanner(System.in);
		
		int hombres;
		int mujeres;
		double total;
		
		System.out.println("Dame la cantidad de hombres");
		hombres = entrada.nextInt();
		System.out.println("Dame la cantidad de mujeres");
		mujeres = entrada.nextInt();
		
		total = mujeres + hombres;
		
		System.out.println("La cantidad de hombre es " +hombres);
		System.out.println("La cantidad de mujeres es " +mujeres);
		System.out.println("El total es " +total);
		System.out.println("El porcentaje de hombres es " +(double)(hombres/total*100)+"%");
		System.out.println("El porcentaje de mujeres es " +(double)(mujeres/total*100)+"%");		
		entrada.close();

	}

}
