package competicion;
import java.util.Scanner;

public class Juego2 {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int monedas = 4;
        System.out.println("Bienvenid@ a la máquina tragaperras.");
        System.out.println("Si consigue 2 figuras iguales le devolveremos su dinero.");
        System.out.println("si consigue las 3, Habrá ganado el premio. ");
        System.out.println("¡Buenas suerte!");
        do {
            monedas = monedas - 1;
            int tirada1 = numeroAleatorio(5);
            int tirada2 = numeroAleatorio(5);
            int tirada3 = numeroAleatorio(5);
            nombre(figura(tirada1), figura(tirada2), figura(tirada3));
            monedas = monedas+moneda(tirada1, tirada2, tirada3);
            imprimir(tirada1,tirada2,tirada3);
            System.out.println("Tienes " + monedas + " monedas");
            System.out.println("Presiona enter para continuar");
            teclado.nextLine();
        } while (monedas != 0);
        if(monedas==0) {
            System.out.println("Vaya... Ha perdido, no se desanime a la siguiente tendrá más suerte...");
        }
        teclado.close();
    }

    public static int numeroAleatorio(int numMaximo) {
        int numero = 0;
        do {
            numero = (int) (Math.random() * numMaximo + 1);
        } while (numero > numMaximo);
        return numero;
    }
public static String figura(int num) {
        String cadena = ("");
        if (num == 1) {
            cadena = "corazon";
        }
        if (num == 2) {
            cadena = "diamante";
        }
        if (num == 3) {
            cadena = "herradura";
        }
        if (num == 4) {
            cadena = "campana";
        }
        if (num == 5) {
            cadena = "limón";
        }
        return cadena;
    }

    public static void nombre(String nombre1, String nombre2, String nombre3) {
        System.out.println("---------------------------------------------");
        System.out.println("| " + nombre1 + "  " + nombre2 + "  " + nombre3 + " |");
        System.out.println("---------------------------------------------");
        
    }
    public static void imprimir(int num1, int num2, int num3) {
        if (num1 == num2 && num2 == num3) {
            System.out.println("Enhorabuena ha ganado 10 monedas");
        } else if (num1 == num2 || num1 == num3 ||
        		num2 == num3) {
            System.out.println("Bien, ha recuperado su moneda");
        } else {
            System.out.println("Lo siento, ha perdido");
        }
    }

    public static int moneda(int num1, int num2, int num3) {
        int monedas = 0;
        if (num1 == num2 && num2 == num3) {
            monedas = monedas + 10;
        } else if (num1 == num2 || num1 == num3 || num2 == num3) {
            monedas++;
        }
        return monedas;
    }
}