package ejer27porcentajevocales;

import java.util.Scanner;

public class Ejer27porcentajevocales {

	public static void main(String[] args) {
		// Programa  que  pide  al  usuario  una  cadena  de  texto,  la  pasa  a  minúsculas,  y  muestra  los 
		//caracteres  de  la  cadena  por  separado.  (Ejercicio  11)  Además  quiero  que  me  diga  el 
		//porcentaje que hay de cada una de las vocales, respecto al total de caracteres de la cadena
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine().toLowerCase();
		int longitud =texto.length();
		int cuentaA=0;
		int cuentaE=0;
		int cuentaI=0;
		int cuentaO=0;
		int cuentaU=0;
		for (int i=0;i<longitud;i++) {
			System.out.print(texto.charAt(i)+ " ");
			if (texto.charAt(i)=='a') {
				cuentaA++;
			} else if (texto.charAt(i)=='e') {
				cuentaE++;
			}else if (texto.charAt(i)=='i') {
				cuentaI++;
			}else if (texto.charAt(i)=='o') {
				cuentaO++;
			}else if (texto.charAt(i)=='u') {
				cuentaU++;
			}
			
			
		}
		System.out.println();
		System.out.println("El porcentaje de A es "+ ((double)cuentaA/longitud*100)+"%");
		System.out.println("El porcentaje de E es "+ ((double)cuentaE/longitud*100)+"%");
		System.out.println("El porcentaje de I es "+ ((double)cuentaI/longitud*100)+"%");
		System.out.println("El porcentaje de O es "+ ((double)cuentaO/longitud*100)+"%");
		System.out.println("El porcentaje de U es "+ ((double)cuentaU/longitud*100)+"%");
		
		input.close();

	}

}
