package ejer20vecescaracterentexto;

import java.util.Scanner;

public class Ejer20vecescaracterentexto {

	public static void main(String[] args) {
		// Programa  que  me  cuenta  el  número  de  veces  que  aparece  un  carácter  en  una  cadena  de 
		//texto. El carácter se pedirá por teclado, y posteriormente se pedirá una cadena de texto.  
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un carácter");
		char letra=input.nextLine().charAt(0);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		int longitud=texto.length();
		int contador =0;
		for (int i=0; i<longitud;i++) {
			if(texto.charAt(i)==letra) {
				contador++;
			}
		}
		System.out.println("La letra "+ letra+ " aparece "+contador+" veces");
		
		input.close();
	}

}
