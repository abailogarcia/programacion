package ejer04arbolnavidad;

import java.util.Scanner;

public class Ejer04arbolnavidad {

	public static void main(String[] args) {
		// Crea una aplicación que dibuje una pirámide de asteriscos. Nosotros le pasamos la 
		//altura de la pirámide por teclado. Este es un ejemplo, si introducimos 5 de altura:  
		//	No se trata de centrarse en pintar los asteriscos sino también espacios en blanco.
		
		Scanner input = new Scanner (System.in);
		System.out.println("Dame la altura del árbol de navidad");
		int altura = input.nextInt();
		for (int i=1; i<=altura;i++) {
			int numEspacios=altura -i;
			for (int j=numEspacios;j>0;j--) {
				System.out.print(" ");
				}
			
			int numAsteriscos=i*2-1;
			for (int k=0;k<numAsteriscos;k++) {
				System.out.print("*");
			}
			System.out.println();
		}
		
		input.close();

	}

}
