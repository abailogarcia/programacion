package ejer16maximo;

import java.util.Scanner;

public class Ejer16maximo {

	public static void main(String[] args) {
		// Pedir por teclado un número entero ‘cantidadSueldos’. Posteriormente pedir tantos sueldos 
		//como indica la variable ‘cantidadSueldos’. Finalmente mostrar el sueldo más alto. Utilizar el 
		//tipo de datos adecuado. 
		Scanner input = new Scanner (System.in);
		System.out.println("Introducir el número de sueldos");
		int cantidadSueldos=input.nextInt();
		int max=0;
		for (int i =0;i<cantidadSueldos;i++) {
			System.out.println("Introduce un sueldo");
			int n1=input.nextInt();
			if(n1>max) {
				max=n1;
			}
		}
		System.out.println("El sueldo más alto es "+max);
		
		input.close();
	}

}
