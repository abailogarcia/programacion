package ejer06arbolnavidadinvertido;

import java.util.Scanner;

public class Ejer06arbolnavidadinvertido {

	public static void main(String[] args) {
		// Crea una aplicación que dibuje un triángulo isósceles invertido de tamaño 4. 
		
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce la altura");
		int altura = input.nextInt();
		
		for (int i =altura; i>= 1;i--) {
			int numEspacios=altura-i;
			for (int k=0;k<numEspacios;k++) {
				System.out.print(" ");
				}
			int numAsteriscos=i*2-1;
			for (int j=numAsteriscos;j>=1;j--) {
				System.out.print("*");
				}
			
			System.out.println();	
		}
		
		
		
		input.close();
		
	}

}
