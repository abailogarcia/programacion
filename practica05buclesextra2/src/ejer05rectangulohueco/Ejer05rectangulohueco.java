package ejer05rectangulohueco;

import java.util.Scanner;

public class Ejer05rectangulohueco {

	public static void main(String[] args) {
		// Crea una aplicación que dibuje un cuadrado hueco con asteriscos. 
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce el ancho");
		int ancho=input.nextInt();
		System.out.println("Introduce la altura");
		int altura=input.nextInt();
		
		for (int i =1;i<=altura;i++) {
			if (i==1 || i==altura) {
				for (int j =1;j<=ancho;j++) {
					System.out.print("*");
				}
			}else {
				for (int k =1;k<=ancho;k++) {
					if (k==1 || k==ancho) {
						System.out.print("*");
					}else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
		
		
		input.close();

	}

}
