package ejer14cuantoscaracteresmostrar;

import java.util.Scanner;

public class Ejer14cuantoscaracteresmostrar {

	public static void main(String[] args) {
		//  Programa  que  lee  una  cadena  por  teclado  y  posteriormente  nos  pregunta  cuantos 
		//caracteres del inicio de la cadena queremos mostrar. Después el programa muestra el 
		//número de caracteres indicado con los que comienza la cadena. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		
		System.out.println("¿Cuántos caracteres quieres mostrar");
		int numero = input.nextInt();
		
		System.out.println(texto.substring(0, numero));
		
		
		input.close();

	}

}
