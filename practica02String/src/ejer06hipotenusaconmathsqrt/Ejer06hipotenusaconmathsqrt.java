package ejer06hipotenusaconmathsqrt;

import java.util.Scanner;

public class Ejer06hipotenusaconmathsqrt {

	public static void main(String[] args) {
		// Programa lea la longitud de los catetos de un triángulo rectángulo y calcule la longitud 
		//de la hipotenusa según el teorema de Pitágoras. (Hipotenusa2 = cateto12 + cateto22) Es 
		//necesario usar el método Math.sqrt() para calcular la raíz cuadrada. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el primer cateto");
		double cateto1 = input.nextDouble();
		System.out.println("Introduce el segundo cateto");
		double cateto2 = input.nextDouble();
		
		double hipotenusacuad = ((cateto1*cateto1) + (cateto2*cateto2));
		double hipotenusa = Math.sqrt(hipotenusacuad);
		System.out.println("La hipotenusacuad es "+ hipotenusacuad);
		System.out.println("La hipotenusa es "+ hipotenusa);
		
		
		input.close();

	}

}
