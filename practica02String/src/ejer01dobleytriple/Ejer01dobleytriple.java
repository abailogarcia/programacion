package ejer01dobleytriple;

import java.util.Scanner;

public class Ejer01dobleytriple {

	public static void main(String[] args) {
		// Programa Java que lee un número entero por teclado y obtiene y muestra por pantalla 
		//el doble y el triple de ese número. 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int numero= input.nextInt();
		
		System.out.println("El doble es "+ (numero*2));
		System.out.println("El triple es "+ (numero*3));
		
		
		input.close();
		
	}

}
