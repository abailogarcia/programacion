package ejer15buscaespacioyvuelvelaspalabras;

import java.util.Scanner;

public class Ejer15buscaespacioyvuelvelaspalabras {

	public static void main(String[] args) {
		// Programa que me pide una cadena por teclado, compuesta de dos palabras separadas 
		//por un espacio, y me muestra por consola la misma cadena, pero mostrando primero la 
		//segunda palabra y luego la primera, separadas por un espacio. Pista, hay un método que 
		//me  indica  la  posición  (número)  de  un  carácter  en  una  cadena  de  texto.  Necesito 
		//encontrar el carácter ‘ ‘ (espacio)
		
		Scanner input =new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		
		int espacio = texto.indexOf(" ");
		System.out.println(texto.substring(espacio+1, texto.length()) + " "+ texto.substring(0,espacio));
		
		input.close();
		

	}

}
