package ejer07descomponernumero;

import java.util.Scanner;

public class Ejer07descomponerdividiendoyresto {
	public static void main(String[] args) {
	
	// Programa que pide y lee un número de 3 cifras y muestra cada cifra en una línea distinta. 
			//Primero  vamos  a  leer  el  numero  como  si  fuera  una  cadena  de  texto  (String,  método 
				//	nextLine())  y  mostramos  el  resultado.  Después  volvemos  a  pedir  un  número  pero  los 
				//	leemos  como  un  int  (método  nextInt()).  Pista:  para  decomponer  en  cifras  String  -> 
				//	métodos de String, int -> división y resto de enteros.
	
	Scanner input = new Scanner(System.in);
	
	System.out.println("Dame otro número");
	int numero = input.nextInt();
	
	System.out.println("la primera cifra es " +(numero/100)%10);
	System.out.println("la segunda cifra es " +(numero/10)%10);
	System.out.println("la tercera cifra es " +(numero%10));
	
	
	input.close();

	
	}
}
