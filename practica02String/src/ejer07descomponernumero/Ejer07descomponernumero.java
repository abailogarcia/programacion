package ejer07descomponernumero;

import java.util.Scanner;

public class Ejer07descomponernumero {

	public static void main(String[] args) {
		// Programa que pide y lee un número de 3 cifras y muestra cada cifra en una línea distinta. 
		//Primero  vamos  a  leer  el  numero  como  si  fuera  una  cadena  de  texto  (String,  método 
			//	nextLine())  y  mostramos  el  resultado.  Después  volvemos  a  pedir  un  número  pero  los 
			//	leemos  como  un  int  (método  nextInt()).  Pista:  para  decomponer  en  cifras  String  -> 
			//	métodos de String, int -> división y resto de enteros.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número de 3 cifras");
		String numero = input.nextLine();
		for (int i =0; i<=(numero.length()-1);i++) {
			System.out.println("El caracter " + (i+1)+ " es " + numero.charAt(i));
		}
		System.out.println("Dame otro número");
		int numero2 = input.nextInt();
		String cadena = String.valueOf(numero2);
		for (int j =0; j<=(cadena.length()-1);j++) {
			System.out.println("El caracter " + (j+1)+ " es " + cadena.charAt(j));
		}
		
		
		
		
		input.close();

	}

}
