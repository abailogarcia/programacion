package ejer09descomponernumeropiramidehaciaatras;

import java.util.Scanner;

public class Ejer09descomponernumeropiramidehaciaatras {

	public static void main(String[] args) {
		// Programa que lea un número entero de 5 cifras y muestre sus cifras desde el final igual 
		//que en el ejemplo. Utiliza la división de enteros. 
		//5 
		//45 
		//345 
		//2345 
		//12345
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un número de 5 cifras");
		int numero = input.nextInt();
		
		System.out.println(numero%10);
		System.out.println((numero%100));		
		System.out.println((numero%1000));
		System.out.println((numero%10000));
		System.out.println((numero%100000));
		
		input.close();

	}

}
