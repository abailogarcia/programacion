package ejer16muestralamitaddecaracteres;

import java.util.Scanner;

public class Ejer16muestralamitaddecaracteres {

	public static void main(String[] args) {
		//  Programa que lee una cadena de texto por teclado y me muestra la segunda mitad de 
		//la cadena. Por ejemplo si una cadena contiene 15 caracteres, me mostrará los 7  últimos.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		int longitud = texto.length();
		
		System.out.println(texto.substring(longitud/2));
		
		
		input.close();
	}

}
