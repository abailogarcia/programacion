package ejer04longitudareacircunferencia;

import java.util.Scanner;

public class Ejer04longitudareacircunferencia {

	public static void main(String[] args) {
		// Programa que lee por teclado el valor del radio de una circunferencia y calcula y muestra 
		//por  pantalla  la  longitud  y  el  área  de  la  circunferencia.  Longitud  de  la  circunferencia  = 
			//	2*PI*Radio, Área de la circunferencia = PI*Radio2 
		Scanner input =new Scanner(System.in);
		System.out.println("Introduce el radio de la circunferencia");
		double radio = input.nextDouble();
		double longitud = 2*3.1416*radio;
		double area = 3.1416*radio*radio;
		
		System.out.println("Su longitud es " + longitud);
		System.out.println("Su área es "+ area);
		
		
		
		
		input.close();
		

	}

}
