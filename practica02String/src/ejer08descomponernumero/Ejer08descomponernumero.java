package ejer08descomponernumero;

import java.util.Scanner;

public class Ejer08descomponernumero {

	public static void main(String[] args) {
		// Programa que lea un número entero de 5 cifras y muestre sus cifras desde el principio 
		//como  en  el  ejemplo.  Utiliza  la  división  de  enteros  y  posteriormente  realiza  lo  mismo 
		//pero tratándolo como un String, y aplicando métodos de String. 
		//1 
		//12 
		//123 
		//1234 
		//12345 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número de 5 cifras");
		int numero = input.nextInt();
		
		System.out.println((numero/10000));
		System.out.println((numero/1000));
		System.out.println((numero/100));
		System.out.println((numero/10));
		System.out.println(numero);
		input.nextLine();
		System.out.println("Dame otro numero");
		String numero2 = input.nextLine();
		
		System.out.println(numero2.charAt(0));
		System.out.println(numero2.substring(0, 2));
		System.out.println(numero2.substring(0, 3));
		System.out.println(numero2.substring(0, 4));
		System.out.println(numero2);
		
		input.close();
	}

}
