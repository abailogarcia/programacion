package ejer03convertirfecha;

import java.util.Scanner;

public class Ejer03convertirfecha {

	public static void main(String[] args) {
		// Programa que pida la fecha de nacimiento. El programa pedirá primero el número, luego 
		//el  mes  escrito  (Enero,  Febrero,  etc.)  y  finalmente  el  año  en  número.  Debe  mostrar  la 
		//fecha  completa  en  una  sola  línea  con  formato:  dia/mes/año.  Al  leer  el  mes  quizás 
		//tengamos un problema -> Revisar apuntes sobre vaciar el buffer de lectura, en la clase 
		//Scanner.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca el año de nacimiento");
		int año = input.nextInt();
		input.nextLine();
		System.out.println("Introduzca el mes de nacimiento");
		String mes = input.nextLine();
		System.out.println("Introduzca el día de nacimiento");
		int dia = input.nextInt();
		
		System.out.println("Tu fecha de nacimiento es " + dia +"/"+mes+"/"+año);
		
		
		
		input.close();
		

	}

}
