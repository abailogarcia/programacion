package ejer05comparasidoscadenasoniguales;

import java.util.Scanner;

public class Ejer05comparasidoscadenasoniguales {

	public static void main(String[] args) {
		// Programa que pide y lee 2 cadenas de texto por teclado y me dice si son iguales o no lo 
		//son (Operador ?). 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la primera cadena");
		String cadena1 = input.nextLine();
		System.out.println("Introduce la segunda cadena");
		String cadena2 = input.nextLine();
		

		System.out.println(cadena1.equals(cadena2)?"Las cadenas son iguales":"Las cadenas son distintas");
		
		
		input.close();
		
		
		
	}

}
	