package ejer10numerosuerte;

import java.util.Scanner;

public class Ejer10numerosuerte {

	public static void main(String[] args) {
		// Programa que pida por teclado la fecha de nacimiento de una persona (día,
		// mes, año) y
		// calcule su número de la suerte. La fecha se pide de forma separada, primero
		// días, luego
		// meses, etc. El número de la suerte se calcula sumando el día, mes y año de la
		// fecha de
		// nacimiento y a continuación sumando las cifras obtenidas en la suma.
		// Si la fecha de nacimiento es 12/07/1980, calculamos el número de la suerte
		// así:
		// 12+7+1980 = 1999 -> 1+9+9+9 = 28

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el día de tu nacimiento");
		int dia=input.nextInt();
		System.out.println("Introduce el mes de tu nacimiento");
		int mes=input.nextInt();
		System.out.println("Introduce el año de tu nacimiento");
		int anno=input.nextInt();
		int suma = dia + mes + anno;
		int sumaTotal = 0;
		
		sumaTotal = sumaTotal + (suma/1000)%10;
		sumaTotal = sumaTotal + (suma/100)%10;
		sumaTotal = sumaTotal + (suma/10)%10;
		sumaTotal = sumaTotal + suma%10;
		
		System.out.println(sumaTotal);
		

		input.close();

	}

}
