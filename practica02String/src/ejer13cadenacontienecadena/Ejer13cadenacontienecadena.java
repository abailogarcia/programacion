package ejer13cadenacontienecadena;

import java.util.Scanner;

public class Ejer13cadenacontienecadena {

	public static void main(String[] args) {
		// Programa que lee dos cadenas por teclado y me dice si la primera está contenida en la 
		//segunda. (Ej: la cadena “hola papa” contiene la cadena “papa”)
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto1 = input.nextLine();
		System.out.println("Introduce otro texto");
		String texto2 = input.nextLine();
		
		System.out.println(texto1.contains(texto2)? 
				"El segundo texto está dentro del primero":
				"El segundo texto no está dentro del primero"
				);
		
		input.close();

	}

}
