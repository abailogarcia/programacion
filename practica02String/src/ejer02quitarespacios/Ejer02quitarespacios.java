package ejer02quitarespacios;

import java.util.Scanner;

public class Ejer02quitarespacios {

	public static void main(String[] args) {
		// Programa  que lee  una  cadena  compuesta  de  varias  palabras  por  teclado  y  la  muestra 
		//quitándole los espacios. Emplear métodos de String (reemplazar caracteres). 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca una cadena");
		String texto = input.nextLine();
		
		String textosin = texto.replaceAll(" ", "");
		System.out.println(textosin);
		
		
		input.close();
		

	}

}
