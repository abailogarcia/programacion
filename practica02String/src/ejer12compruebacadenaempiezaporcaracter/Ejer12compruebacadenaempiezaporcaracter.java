package ejer12compruebacadenaempiezaporcaracter;

import java.util.Scanner;

public class Ejer12compruebacadenaempiezaporcaracter {

	public static void main(String[] args) {
		//  Programa que lee una cadena de texto y posteriormente lee un carácter, y me dice si la 
		//cadena comienza por ese carácter.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		System.out.println("Introduce un caracter");
		char caracter = input.nextLine().charAt(0);
		
		System.out.println(caracter==texto.charAt(0)?
				"El texto empieza por el caracter":
				"El texto no empieza por el caracter");
		
		
		
		input.close();
		
		
	}

}
