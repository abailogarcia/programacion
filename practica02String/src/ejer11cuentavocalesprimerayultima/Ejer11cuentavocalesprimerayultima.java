package ejer11cuentavocalesprimerayultima;

import java.util.Scanner;

public class Ejer11cuentavocalesprimerayultima {

	public static void main(String[] args) {
		// Programa que lee una cadena por teclado y me indica lo siguiente mediante 3 mensajes 
		//diferentes: (Operador ? y Métodos clase String) 
		 //Si tiene alguna vocal o no la tiene 
		 //Si comienza por vocal o no. 
		 //Si termina con vocal o no. 
		Scanner input = new Scanner (System.in);
		System.out.println("Escribe un texto");
		String cadena = input.nextLine();
		
		boolean contieneVocales = cadena.contains("a")||cadena.contains("e")||cadena.contains("i")||cadena.contains("o")||cadena.contains("u");
		System.out.println(contieneVocales ? "El texto contiene vocales": "El texto no contiene vocales");
		
		boolean empiezaVocal = cadena.startsWith("a")||cadena.startsWith("e")||cadena.startsWith("i")||cadena.startsWith("i");
		System.out.println(empiezaVocal? "El texto empieza por vocal":"El texto no empieza por vocal");
		
		boolean terminaVocal = cadena.endsWith("a")||cadena.endsWith("e");
		System.out.println(terminaVocal? "El texto termina por vocal":"El texto no termina por vocal");
		
		input.close();
		
		

	}

}
