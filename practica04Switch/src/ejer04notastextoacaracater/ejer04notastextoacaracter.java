package ejer04notastextoacaracater;

import java.util.Scanner;

public class ejer04notastextoacaracter {

	public static void main(String[] args) {
		// Pedir  una  nota  numérica  entera  entre  0  y  10  y  mostrar  el  valor  de  la  nota:  Insuficiente, 
		//Suficiente,  Bien,  Notable  o  Sobresaliente.  (No  es  necesario  poner  break;  en  todos  los 
		//casos.) 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una nota");
		int opcion = input.nextInt();
				
		switch (opcion) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
				System.out.println("insuficiente");
				break;
			case 5:
				System.out.println("suficiente");
				break;
			case 6:
				System.out.println("bien");
				break;
			case 7:
			case 8:
				System.out.println("notable");
				break;
			case 9:
			case 10:
				System.out.println("sobresaliente");
				break;
			default:
				System.out.println("No es una nota correcta");
		
		
		}
		
		
		input.close();
		

	}

}
