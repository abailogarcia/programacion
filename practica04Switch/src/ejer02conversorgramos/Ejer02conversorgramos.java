package ejer02conversorgramos;

import java.util.Scanner;

public class Ejer02conversorgramos {

	public static void main(String[] args) {
		// Programa  que  pida  al  usuario  una  cantidad  de  gramos  y  posteriormente  pida  seleccionar 
		//una  opción  de  un  menú  con  opciones  numéricas  (opciones  del  menú:  1-Kilogramo,  2-
		//Hectogramos,  3-Decagramos,  4-Decigramos,  5-Centigramos,  6-Miligramos).  Al  seleccionar 
		//una  opción  mostrar  la  conversión  de  gramos  a  dicha  unidad.  Comprobar  si  la  opción  del 
		//menú  introducida  no  es  válida.  Si  la  cantidad  de  gramos  es  un  valor  negativo,  no  hacer 
		//nada. 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cantidad de gramos");
		double gramos = input.nextDouble();
		int opcion =0;
		if (gramos >=0) {
			System.out.println("Introduce una opción para convertir");
			System.out.println("1.- Kilogramos");
			System.out.println("2.- Hectogramos");
			System.out.println("3.- Decagramos");
			System.out.println("4.- Decigramos");
			System.out.println("5.- Centigramos");
			System.out.println("6.- Miligramos");
			opcion = input.nextInt();
			
			switch (opcion) {
				case 1:
					System.out.println(gramos + " gramos son " + (gramos/1000)+ " Kilogramos");
					break;
				case 2:
					System.out.println(gramos + " gramos son " + (gramos/100)+ " Hectogramos");
					break;
				case 3:
					System.out.println(gramos + " gramos son " + (gramos/10)+ " Decagramos");
					break;
				case 4:
					System.out.println(gramos + " gramos son " + (gramos*10)+ " Decigramos");
					break;
				case 5:
					System.out.println(gramos + " gramos son " + (gramos*100)+ " Centigramos");
					break;
				case 6:
					System.out.println(gramos + " gramos son " + (gramos*1000)+ " Miligramos");
					break;
				default:
					System.out.println("No has seleccionado una opción válida");
						
			}
			
		} else {
			System.out.println("No puede ser negativo");
		}
		
		input.close();

	}

}
