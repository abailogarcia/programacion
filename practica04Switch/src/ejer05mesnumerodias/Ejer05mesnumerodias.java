package ejer05mesnumerodias;

import java.util.Scanner;

public class Ejer05mesnumerodias {

	public static void main(String[] args) {
		// Programa que lea un mes en tipo String y compruebe si el valor corresponde a un mes de 
		//30,  31  o  28  días.  Se  mostrará  además  el  nombre  del  mes.  Comprobar  si  la  opción 
		//introducida no es válida.  (No es necesario poner break; en todos los casos.) 

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un mes");
		String mes = input.nextLine();
		String mesmin = mes.toLowerCase();
		
		switch (mesmin) {
			case "enero":
			case "marzo":
			case "mayo":
			case "julio":
			case "agosto":
			case "octubre":
			case "diciembre":
				System.out.println("Tiene 31 días");
				break;
			case "febrero":
				System.out.println("Tiene 28 días");
				break;
			case "abril":
			case "junio":
			case "septiembre":
			case "noviembre":
				System.out.println("Tiene 30 días");
				break;
			default:
				System.out.println("No es correcto");
				//break;
		
		}
		
		
		input.close();
	}

}
