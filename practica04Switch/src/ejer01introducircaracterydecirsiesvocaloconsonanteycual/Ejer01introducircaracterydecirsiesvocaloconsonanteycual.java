package ejer01introducircaracterydecirsiesvocaloconsonanteycual;

import java.util.Scanner;

public class Ejer01introducircaracterydecirsiesvocaloconsonanteycual {

	public static void main(String[] args) {
		// 1a.  Programa  Java  que  lea  un  carácter  en  minúscula  (no  hace  falta  comprobarlo)  y, 
		//utilizando una sentencia switch, diga qué vocal es, o en caso contrario, que indique que no 
		//es  vocal,  y  muestre  el  carácter  en  ambos  casos.  Probar  el  ejercicio  usando  la  sentencia 
		//break  en  los  cases. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un carácter");
		char letra = input.nextLine().charAt(0);
		
		switch (letra) {
		
			case 'a':
				System.out.println("Es una a");
				break;
			case 'e':
				System.out.println("Es una e");
				break;
			case 'i':
				System.out.println("Es una i");
				break;
			case 'o':
				System.out.println("Es una o");
				break;
			case 'u':
				System.out.println(("Es una u"));
				break;
			default:
				System.out.println("Es una consonante");
				System.out.println("Concretamente la " + letra);
				
				
		}
		
		
		input.close();

	}

}
