package ejer06operaciones;

import java.util.Scanner;

public class Ejer06operaciones {

	public static void main(String[] args) {
		// Programa que pida 2 números float y a continuación un carácter de operación
		// aritmética
		// (+, -, *, /, %). Mediante una sentencia switch escoger el caso referente al
		// carácter de
		// operación introducido. El programa mostrará un mensaje con el resultado de la
		// operación
		// escogida, entre los 2 números. Indicar si el carácter introducido no es
		// válido.

		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un float");
		float numero1 = input.nextFloat();

		System.out.println("Introduce otro float");
		float numero2 = input.nextFloat();

		// limpio buffer
		input.nextLine();

		System.out.println("Introduce un caracter de operacion (+.-,*,/,%)");
		char operador = input.nextLine().charAt(0);

		switch (operador) {
		case '+':
			System.out.println(numero1 + numero2);
			break;
		case '-':
			System.out.println(numero1 - numero2);
			break;
		case '*':
			System.out.println(numero1 * numero2);
			break;
		case '/':
			System.out.println(numero1 / numero2);
			break;
		case '%':
			System.out.println(numero1 % numero2);
			break;
		default:
			System.out.println("Operador no reconocido");
			break;
		}

		input.close();
	}

}
