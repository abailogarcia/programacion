package ejer03convertirnumeroatexto;

import java.util.Scanner;

public class ejer03convertirnumerotexto {

	public static void main(String[] args) {
		// Pedir una nota numérica entera entre 0 y 10, y mostrar dicha nota de la forma: cero, uno, 
		//dos, tres... Comprobar si la opción introducida no es válida. 
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número entre 0 y 10");
		int numero = input.nextInt();
		
		switch (numero) {
			case 0:
				System.out.println("cero");
				break;
			case 1:
				System.out.println("uno");
				break;
			case 2:
				System.out.println("dos");
				break;
				
			default: 
				System.out.println("La opción no es válida");
		
		
		}
		
		
		
		input.close();
		

	}

}
