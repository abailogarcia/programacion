package ejercicio30calculadivisores;

import java.util.Scanner;

public class Ejercicio3001 {

	public static void main(String[] args) {
		// Programa que me pide un número entero positivo y me muestra por consola todos
		// los
		// divisores de ese número desde el numero 1 hasta el numero introducido, ambos
		// incluidos.
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número entero positivo");
		int numero = input.nextInt();
		System.out.print("Los números divisores son: ");
		for (int i = 1; i <= numero; i++) {

			if (numero % i == 0) {
				System.out.print(i+ " ");

			}

		}

		input.close();

	}

}
