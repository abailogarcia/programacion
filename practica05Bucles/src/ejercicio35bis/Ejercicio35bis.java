package ejercicio35bis;

import java.util.Scanner;

public class Ejercicio35bis {

	public static void main(String[] args) {
		// Realizar un programa que nos pida un número, y nos diga cuantos números hay entre 1 y n 
		//que son primos. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int numero=input.nextInt();
		
		
		for (int i=1;i<=numero;i++) {
			boolean primo=true;
			for (int j=2;j<i;j++) {
				if(i%j==0) {
					primo=false;
					break;
				}
			}
			if (primo) {
				System.out.println(i);
			}
		}
		
		
		input.close();

	}

}
