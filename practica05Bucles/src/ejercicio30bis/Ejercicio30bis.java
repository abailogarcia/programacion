package ejercicio30bis;

import java.util.Scanner;

public class Ejercicio30bis {

	public static void main(String[] args) {
		// Programa  que  me  pide  un  número  entero  positivo  y  me  muestra  por  consola  todos  los 
		//divisores de ese número desde el numero 1 hasta el numero introducido, ambos incluidos.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int numero = input.nextInt();
		for (int i=2; i<=numero; i++ ) {
			if(numero%i==0) {
				System.out.println(i);
			}
		}
		
		input.close();
		

	}

}
