package ejercicio04;

import java.util.Scanner;

public class Ejercicio0401 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Programa que muestre los números desde un número mayor introducido por
		// teclado hasta
		// otro número menor introducido por teclado, utilizando la instrucción for.
		// Realizar el
		// ejercicio mostrando los números cada uno en una línea. Y después mostrándolos
		// en la
		// misma línea separados por un espacio. Mostrar un mensaje que indique el final
		// del
		// programa.
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca número mayor");
		int n1 = entrada.nextInt();
		System.out.println("Introduzca número menor");
		int n2 = entrada.nextInt();

		for (int i = n1; i >= n2; i--) {
			System.out.println(i);
		}

		for (int i = n1; i >= n2; i--) {
			System.out.print(i + " ");
		}

		System.out.println("");
		System.out.println("Se acabó");
		entrada.close();

	}

}
