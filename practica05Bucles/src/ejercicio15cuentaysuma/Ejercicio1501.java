package ejercicio15cuentaysuma;

import java.util.Scanner;

public class Ejercicio1501 {

	public static void main(String[] args) {
		// Programa que pide números enteros positivos y negativos por teclado hasta que 
		//introducimos el número 0. Después deberá indicar el número total de números leídos y la 
		//suma total de sus valores. 
		
		Scanner input = new Scanner(System.in);
		
		int contador =0;
		int suma = 0;
		int numero=1;
		while (numero !=0) {
			System.out.println("Introduce un número");
			System.out.println("0 para salir");
			numero = input.nextInt();
			contador ++;
			suma = suma + numero;
			
		}
		System.out.println("Has introducido "+ contador+ " números");
		System.out.println("La suma de los " + contador+ " números es "+suma);
		
		input.close();

	}

}
//Con un do while
//int contador =0;
//int suma = 0;
//int numero=0;
//do {
	//System.out.println("Introduce un número");
	//numero = input.nextInt();
	//contador ++;
	//suma = suma + numero;
	//}
//while (numero !=0);

