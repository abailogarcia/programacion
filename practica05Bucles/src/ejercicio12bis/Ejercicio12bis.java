package ejercicio12bis;

import java.util.Scanner;

public class Ejercicio12bis {

	public static void main(String[] args) {
		// Programa  que  muestra  un  menú  con  3  opciones.  1  –  Opción  1,  2  –  Opción  2,  3  –  Salir. 
		//Mientras que se introduzcan las opciones 1 y 2 debe mostrar un mensaje indicando la opción 
		//seleccionada, y acto seguido vuelve a mostrar el menú. Si se selecciona salir, mostrará un 
		//mensaje de despedida y terminará el programa. Usar switch para obtener la selección. 
		Scanner input = new Scanner(System.in);
		int opcion;		
		do {
			System.out.println("1.-Opción 1");
			System.out.println("2.-Opción 2");
			System.out.println("3.-Salir");
			System.out.println("Introduzca una opción");
			opcion =input.nextInt();
				
			switch (opcion) {
					
				case 1:
					System.out.println("Has seleccionado la opción 1");
					break;
				case 2:
					System.out.println("Has seleccionado la opción 2");
					break;
				case 3:
					System.out.println("Adios");
					break;
			}
		} while (opcion!=3 );
		System.out.println("Se acabó");
		input.close();
		

	}

}
