package ejercicio29bis;

import java.util.Scanner;

public class Ejercicio29bis {

	public static void main(String[] args) {
		//  Programa  que  pida  cadenas  de  texto  hasta  introducir  la  cadena  de  texto  “fin”. 
		//Posteriormente me dirá la cantidad de vocales (mayúsculas o minúsculas) y el porcentaje de 
		//vocales respecto al total de caracteres. Además me mostrará la cadena resultante de unir 
		//todas las cadenas separadas por un espacio. 
		
		Scanner input = new Scanner(System.in);
		String texto;
		int cantidad =0;
		String textoTotal="";
		do {
			System.out.println("Introduce un texto");
			texto = input.nextLine().toLowerCase();
			int longitud = texto.length();
			
			for (int i =0; i<longitud;i++) {
				char caracter = texto.charAt(i);
				if (caracter=='a' || caracter=='e' ||caracter=='i' ||caracter=='o' ||caracter=='u') {
					cantidad++;
				}
				
			}
						
			textoTotal=textoTotal+" "+texto;
			
		} while (!texto.equals("fin"));
		int longitudTotal=textoTotal.length()-1;
		System.out.println(longitudTotal);
		System.out.println(textoTotal);
		
		System.out.println("El texto tiene "+ cantidad + " vocales que son un " +((double)cantidad/longitudTotal)*100+ "% del total");
		input.close();

	}

}
