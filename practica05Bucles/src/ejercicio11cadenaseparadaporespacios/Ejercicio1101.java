package ejercicio11cadenaseparadaporespacios;

import java.util.Scanner;

public class Ejercicio1101 {

	public static void main(String[] args) {
		// Programa  que  pide  al  usuario  una  contraseña  formada  por  una  cadena  con  cualquier 
		//carácter, y muestra los caracteres de la cadena de texto separados por un espacio.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca un password");
		String pass = input.nextLine();
		
		int longitud = pass.length();
		
		//for (int i =0; i<pass.length(); i++) {
		for (int i =0; i<longitud; i++) {
			System.out.print((pass.charAt(i))+ " ");
			
		}
		
		input.close();
	}

}
