package ejercicio37numerosperfectoshasta1000;

import java.util.Scanner;

public class Ejercicio37numerosperfectoshasta1000 {

	public static void main(String[] args) {
		// Mostrar los números perfectos entre 1 y 1000.

		Scanner input = new Scanner(System.in);

		for (int i = 1; i <= 1000; i++) {
			int suma = 0;
			for (int j = 1; j < i; j++) {
				if (i % j == 0) {
					// System.out.println(j);
					suma = suma + j;
				}
				
			}
				if (suma == i) {
					System.out.println("El " + i + " es perfecto");
				}
		}

		input.close();
	}

}
