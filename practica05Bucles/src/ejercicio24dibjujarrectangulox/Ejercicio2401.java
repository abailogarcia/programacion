package ejercicio24dibjujarrectangulox;

import java.util.Scanner;

public class Ejercicio2401 {

	public static void main(String[] args) {
		// Programa que pida el largo y el ancho (valores enteros) y dibuje un rectángulo, con tantas 
		//‘X’ por alto y por ancho como se han introducido por teclado. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el ancho");
		int ancho = input.nextInt();
		System.out.println("Dame el alto");
		int alto = input.nextInt();
		
		for (int i=0; i<alto; i++) {
			for (int j=0; j<ancho; j++) {
				System.out.print("X ");
				
			}
			System.out.println();
		}
		
		
		
		input.close();
		

	}

}
