package ejercicio16formaximo;

import java.util.Scanner;

public class Ejercicio1601 {

	public static void main(String[] args) {
		//  Pedir por teclado un número entero ‘cantidadSueldos’. Posteriormente pedir tantos sueldos 
		//como indica la variable ‘cantidadSueldos’. Finalmente mostrar el sueldo más alto. Utilizar el 
		//tipo de datos adecuado.

		Scanner input = new Scanner(System.in);
		System.out.println("Dime el número de sueldos a introducir");
		int cantidadSueldos = input.nextInt();
		
		//uso float valor literal
		float masalto =0.0F;
		for (int i =1 ; i<= cantidadSueldos; i++) {
			System.out.println("Introduce el sueldo número " + i);
			float sueldo = input.nextFloat();
			if (sueldo > masalto) {
				masalto= sueldo;
			}
		}
		System.out.println("El sueldo más alto es " + masalto);
		
		input.close();
		
		
	}

}
