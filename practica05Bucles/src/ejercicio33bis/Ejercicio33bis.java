package ejercicio33bis;

public class Ejercicio33bis {

	public static void main(String[] args) {
		// Necesitamos mostrar un contador con 5 dígitos (X-X-X-X-X), que muestre los números del 0-
		//0-0-0-0  al  9-9-9-9-9.  Después  añadirle  una  modificación  para  que  en  lugar  de  mostrar  el 
		//número 3 lo cambie por la letra ‘E’. 
		
		for (int i =0; i<=9;i++) {
			for (int j=0; j<=9; j++) {
				for (int k=0; k<=9; k++) {
					for (int l=0; l<=9; l++) {
						for (int m=0; m<=9; m++) {
							//System.out.println(i+"-"+j+"-"+k+"-"+l+"-"+m);
							String texto = i+"-"+j+"-"+k+"-"+l+"-"+m;
							System.out.println(texto.replaceAll("3", "E"));
						}
					}
				}
			}
		}
	}

}
