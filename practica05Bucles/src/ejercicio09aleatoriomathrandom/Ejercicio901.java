package ejercicio09aleatoriomathrandom;

public class Ejercicio901 {

	public static void main(String[] args) {
		// Generar un número entero aleatorio entre 0 y 100 ayudándome del método Math.random(). 
		//Posteriormente muestro por pantalla todos los números desde el número aleatorio hasta el 
		//-100, de 7 en 7.
		
		System.out.println("Voy a generar un número aleatorio");
		double aleatorio;
		aleatorio = Math.random()*101;
		System.out.println("El aleatorio es " + aleatorio);
		
		for (int i= (int)aleatorio; i>=-100; i-=7) {
			System.out.println(i);
		}
		
	}

}
