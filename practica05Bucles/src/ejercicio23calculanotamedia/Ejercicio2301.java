package ejercicio23calculanotamedia;

import java.util.Scanner;

public class Ejercicio2301 {

	public static void main(String[] args) {
		// Programa para corregir exámenes. Se introducen las notas del alumno de cada
		// examen
		// (double) hasta que se introduzca un número negativo, y calcular la nota
		// media.

		Scanner input = new Scanner(System.in);

		double suma = 0;
		int cantidad = 0;
		double nota;

		do {
			System.out.println("Dame la nota del alumno");
			nota = input.nextDouble();
			if (nota >= 0) {
				suma = suma + nota;
				cantidad++;
			}

		} while (nota >= 0);

		if (cantidad != 0) {
			System.out.println("La media es "+ (suma/cantidad));
		}

		input.close();

	}

}
