package ejercicio38juegoadivinarnumero;

import java.util.Scanner;

public class Ejercicio3801 {

	public static void main(String[] args) {
		// Realizar un juego para adivinar un número. Para ello se genera un número aleatorio de 1 al 
		//100 y luego se va pidiendo números indicando “mayor” o “menor” según sea mayor o menor 
		//con respecto al número aleatorio generado. El proceso termina cuando el usuario acierta. 

		
		//si lo casteo corta decimales 
		//Math.round redondea
		
		Scanner input =new Scanner(System.in);
		
		int aleatorio=(int)Math.round((Math.random()*100));
			
		int numero;
		
		
		do { 
			System.out.println("Introduzca un número del 1 al 100 y suerte");
			numero = input.nextInt();
			if (numero == aleatorio) {
			System.out.println("Enhorabuena, el número es "+ aleatorio);
			} else if (numero > aleatorio){
				System.out.println("Error, el número es menor");
				} else {
					System.out.println("Error, el número es mayor");
				} 
						
		} while (numero != aleatorio);
		
		
		
		
		input.close();
	}

}
