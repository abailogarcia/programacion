package ejercicio20cuentacaracterrepetidoencadena;

import java.util.Scanner;

public class Ejercicio2001 {

	public static void main(String[] args) {
		// Programa  que  me  cuenta  el  número  de  veces  que  aparece  un  carácter  en  una  cadena  de 
		//texto. El carácter se pedirá por teclado, y posteriormente se pedirá una cadena de texto.

		Scanner input = new Scanner(System.in);
		System.out.println("Dime el carácter a buscar");
		char caracter = input.nextLine().charAt(0);
		System.out.println("Introduce el texto base");
		String texto = input.nextLine();
		int contador = 0;
		
		
		//cadena.length() --> longitud de la cadena
		//comparar números -> if (numero==3)
		//comparar char -> if (cadena.charAt(i)=='a')
		//comparar string -> if (cadena.equals(cadena1))
		for( int i=0; i< texto.length(); i++) {
			if (texto.charAt(i)==caracter) {
				contador++;
			}
			
			
		}
		System.out.println("Dentro del texto hay " + contador +" "+ caracter+ "s");
		input.close();
		
	}

}
