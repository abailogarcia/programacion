package ejercicio07tablamultiplicar;

import java.util.Scanner;

public class Ejercicio0701 {

	public static void main(String[] args) {
		// Mostrar la tabla de multiplicar de un numero introducido por teclado del 1 al 10.
		
		Scanner entrada =new Scanner(System.in);
		System.out.println("Introduce un número");
		int numero = entrada.nextInt();
		
		System.out.println("Tabla del numero "+numero+ "\n");  // para un intro \n
		
		for (int i=1; i <=10 ; i++) {
			System.out.println(numero+ " x " +i+ " = " +(i*numero));
			
		}
		
		
		
		entrada.close();

	}

}
