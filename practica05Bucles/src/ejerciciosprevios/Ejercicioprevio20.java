package ejerciciosprevios;

public class Ejercicioprevio20 {

	public static void main(String[] args) {
		// Bucle for
		for (int i=0; i<5; i++) {
			System.out.println("Esto es una frase");
		}
		
		for (int i=0; i<5; i++) {
			System.out.println("Esto es una frase"+i);
		}
		
		for (int i=1; i<7; i++) {
			System.out.println("Esto es otra frase"+i);
		}
		//ámbito de las variables
		//una variable declarada dentro del main, será válida para todo el main
		//una variable declarada dentro de un for, sólo será válida en ese for
		for (int i=4; i>=0; i--) {
			System.out.println("Esto es otra frase"+i);
		}
		for (int i=0; i<10; i+=2) {
			System.out.println("Esto es una frase"+i);
		}
	}

}
