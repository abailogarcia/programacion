package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio6dowhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada = new Scanner(System.in);
		System.out.println("Dame una cantidad (menor que 10)");
		int cantidad =entrada.nextInt();
		do {
			System.out.println("La cantidad es "+ cantidad);
			cantidad +=1;
			//cantidad ++;
			//cantidad = cantidad +1;
		} while(cantidad <10);
		entrada.close();

	}

}
