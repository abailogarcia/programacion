package ejercicio35cuantosprimoshastaunnumero;

import java.util.Scanner;

public class Ejercicio35cuantosprimoshastaunnumero {

	public static void main(String[] args) {
		// Realizar un programa que nos pida un número, y nos diga cuantos números hay
		// entre 1 y n
		// que son primos.

		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número");
		int numero = input.nextInt();
		System.out.println("Los números primos entre 1 y " + numero + " son:");
		int cuentaPrimos=0;
		for (int i = 1; i <= numero; i++) {
			int contador = 0;
			for (int j = 2; j < i; j++) {
				if ((i % j) == 0) {
					contador++;
				}

			}
			if (contador == 0) {
				System.out.println(i);
				cuentaPrimos++;
			}
		
		}
		System.out.println("Hay " + cuentaPrimos + " números primos");
		input.close();
	}

}
