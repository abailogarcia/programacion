package ejercicio25bis;

import java.util.Scanner;

public class Ejercicio25bis {

	public static void main(String[] args) {
		// Pasar  de  grados  centígrados  a  grados  kelvin.  El  proceso  de  leer  grados  centígrados  y 
		//convertirlos se debe repetir mientras que se responda ‘S’ a la pregunta: ¿Repetir proceso? 
		//		(S/N). (Conversión: ºK = ºC + 273) 

		Scanner input = new Scanner(System.in);
		String opcion;
		do {
		System.out.println("Introduzca grados centígrados");
		
		double centi=input.nextDouble();
		opcion=input.nextLine();
		System.out.println("Son " +(centi + 273)+ " grados Kelvin");
		System.out.println("");
		System.out.println("Repetir proceso S/N");
	
		opcion = input.nextLine().toLowerCase();
		} while (opcion.equals("s"));
		System.out.println("Se acabó");
		input.close();
	}

}
