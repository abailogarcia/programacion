package ejercicio39mediapositivosynegativos;

import java.util.Scanner;

public class Ejercicio39mediapositivosynegativos {

	public static void main(String[] args) {
		// Pedir  10  números.  Mostrar  la  media  de  los  números  positivos,  la  media  de  los  números 
		//negativos y la cantidad de ceros. 
		Scanner input = new Scanner(System.in);
		
			int sumapositivo=0;
			int cuentapositivos=0;
			int sumanegativo=0;
			int cuentanegativos=0;
			int cuentaceros=0;
		for (int i= 0 ; i<10; i++) {
			System.out.println("Dame un número entero");
			int numero = input.nextInt();
			
				if (numero >0) {
					sumapositivo =+ numero;
					cuentapositivos++;
					} else if ( numero <0) {
						sumanegativo =+ numero;
						cuentanegativos++;
					} else {
						cuentaceros++;
					}
			
			}
		
		System.out.println("La media de los números positivos es "+ ((double)sumapositivo/cuentapositivos));
		System.out.println("La media de los números negativos es "+ ((double)sumanegativo/cuentanegativos));
		System.out.println("Has introducido  "+ cuentaceros +" ceros");
		
		
		input.close();
		
		
	}

}
