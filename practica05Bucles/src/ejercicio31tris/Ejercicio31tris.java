package ejercicio31tris;

import java.util.Scanner;

public class Ejercicio31tris {

	public static void main(String[] args) {
		// Comprobar si un numero introducido por teclado es perfecto. Un número es
				// perfecto si es
				// igual a la suma de todos sus divisores positivos sin incluir el propio
				// número. (El 6 es perfecto
				// tiene como divisores: 1, 2, 3 y 6 pero el 6 no se cuenta como divisor para
				// comprobar si es perfecto: 1+2+3=6)
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce un número");
		int numero=input.nextInt();
		int suma=0;
		for(int i=1;i<numero;i++) {
			if(numero%i==0) {
				suma+=i;
			}
		}
		if (suma==numero) {
			System.out.println("El número "+ numero+" es perfecto");
		}else {
			System.out.println("El número "+ numero+" no es perfecto");
		}
		
		input.close();

	}

}
