package ejercicio02;

public class Ejercicio0102 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Programa que muestre los números del 100 al 1 utilizando la instrucción
		// while. Realizar el
		// ejercicio mostrando los números cada uno en una línea. Y después mostrándolos
		// en la
		// misma línea separados por un espacio. Mostrar un mensaje que indique el final
		// del
		// programa.

		int cantidad = 100;
		while (cantidad > 0) {
			System.out.println(cantidad);
			cantidad = cantidad - 1;
		}
		cantidad = 100;
		while (cantidad > 0) {
			System.out.print(cantidad + " ");
			cantidad = cantidad - 1;
		}
		System.out.println("");
		System.out.println("Se acabó");

	}

}
