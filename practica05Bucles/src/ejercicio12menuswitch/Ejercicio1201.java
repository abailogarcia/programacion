package ejercicio12menuswitch;

import java.util.Scanner;

public class Ejercicio1201 {

	public static void main(String[] args) {
		// Programa  que  muestra  un  menú  con  3  opciones.  1  –  Opción  1,  2  –  Opción  2,  3  –  Salir. 
		//Mientras que se introduzcan las opciones 1 y 2 debe mostrar un mensaje indicando la opción 
		//seleccionada, y acto seguido vuelve a mostrar el menú. Si se selecciona salir, mostrará un 
		//mensaje de despedida y terminará el programa. Usar switch para obtener la selección.

		Scanner input = new Scanner(System.in);
		
		int opcion=0;
		do {
			System.out.println("1.- Opcion1");
			System.out.println("2.- Opcion2");
			System.out.println("3.- Opcion3");
			opcion= input.nextInt();
			switch (opcion) {
			
			case 1:
				System.out.println("Opción 1");
				break;
				
			case 2:
			System.out.println("Opción 2");
			break;
			
			case 3:
			System.out.println("Adiós");
			break;
				
			}
		}
			while (opcion !=3);
		
		
		
		input.close();
		
		
		}

}
