package ejercicio28mayormenorcantidad;

import java.util.Scanner;

public class Ejercicio2801 {

	public static void main(String[] args) {
		// TPrograma que pida número enteros, hasta introducir el número 0. En ese
		// momento indicar
		// cuál es el mayor, el menor y la cantidad de números leída, sin contar el 0.

		Scanner input = new Scanner(System.in);
		// System.out.println("Introduzca un número entero o 0 para terminar");

		int numero;
		int mayor = 0;
		int menor = Integer.MAX_VALUE;
		int contador = 0;

		do {
			System.out.println("Introduzca un número entero o 0 para terminar");
			numero = input.nextInt();

			if (numero > mayor) {
				mayor = numero;
			}
			if (numero < menor && numero != 0) {
				menor = numero;
			}
			contador++;
			
		} while (numero != 0);
		System.out.println("El máximo ha sido " + mayor);
		System.out.println("El mínimo ha sido " + menor);
		System.out.println("Has introducido " + (contador -1) + " números");
		System.out.println("Se acabó");

		input.close();

	}

}
