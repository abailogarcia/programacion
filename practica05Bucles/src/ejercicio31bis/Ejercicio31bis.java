package ejercicio31bis;

import java.util.Scanner;

public class Ejercicio31bis {

	public static void main(String[] args) {
		// Comprobar si un numero introducido por teclado es perfecto. Un número es perfecto si es 
		//igual a la suma de todos sus divisores positivos sin incluir el propio número. (El 6 es perfecto 
			//	tiene como divisores: 1, 2, 3 y 6 pero el 6 no se cuenta como divisor para comprobar si es 
				//perfecto: 1+2+3=6) 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int entero = input.nextInt();
		int suma =0;
		for (int i=1;i<entero;i++) {
			if (entero%i==0) {
				suma +=i;
			}
		}
		if (suma == entero) {
			System.out.println(entero +" es perfecto");
			
		}else {
			System.out.println(entero +" no es perfecto");
		}
		System.out.println(suma);
		input.close();
	}

}
