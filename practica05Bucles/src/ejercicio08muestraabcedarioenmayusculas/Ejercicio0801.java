package ejercicio08muestraabcedarioenmayusculas;

public class Ejercicio0801 {

	public static void main(String[] args) {
		// Programa que muestra el abecedario en castellano en mayúsculas.
		//con int
		//hay que castear la i para que muestre un char
		//(char)i
		
		for (int i =65; i<=90;i++) {
			System.out.println((char)i);
			if (i=='N') {
				System.out.println("Ñ");
			}
		}
		
		//con char
		for (char i='A';i<='Z';i++) {
			System.out.println(i);
			if (i=='N') {
				System.out.println("Ñ");
			}
		}
		
	}

}
