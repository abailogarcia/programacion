package ejercicio36bis;

import java.util.Scanner;

public class Ejercicio36bis {

	public static void main(String[] args) {
		// Programa que me cuenta el número de palabras que tiene una cadena de texto. Entendemos 
		//que las palabras están separadas por un único espacio. 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		int longitud = texto.length();
		int contador =0;
		for (int i=0; i<longitud;i++) {
			if (texto.charAt(i)==' ') {
				contador ++;
			}
			
		}
		
		System.out.println("El texto tiene "+ (contador +1)+ " palabras");
		input.close();
		
		
	}

}
