package ejercicio26bis;

import java.util.Scanner;

public class Ejercicio26bis {

	public static void main(String[] args) {
		// Programa que pide un numero entero positivo desde el 0 a 255 y muestra su conversión a 
	//	número binario. Debemos crear nuestro propio algoritmo, sin usar clases nuevas. Podemos 
		//usar un String para almacenar los bits que voy obteniendo de las sucesivas divisiones.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int numero = input.nextInt();
		String binario="";
		int resto =0;
		int cociente=0;
		do {
			cociente=numero/2;
			resto = numero%2;
			numero= cociente;
			binario = (String.valueOf(resto)) + binario;
			
		}while (numero>0);
		System.out.println(binario);
		input.close();

	}

}
