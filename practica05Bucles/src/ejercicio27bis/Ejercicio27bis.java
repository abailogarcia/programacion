package ejercicio27bis;

import java.util.Scanner;

public class Ejercicio27bis {

	public static void main(String[] args) {
		// Programa  que  pide  al  usuario  una  cadena  de  texto,  la  pasa  a  minúsculas,  y  muestra  los 
		//caracteres  de  la  cadena  por  separado.  (Ejercicio  11)  Además  quiero  que  me  diga  el 
		//porcentaje que hay de cada una de las vocales, respecto al total de caracteres de la cadena.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine().toLowerCase();
		int longitud = texto.length();
		String textoseparado="";
		int cuentaA=0;
		int cuentaE=0;
		int cuentaI=0;
		int cuentaO=0;
		int cuentaU=0;
		for (int i =0; i<longitud; i++) {
			char caracter=texto.charAt(i);
			textoseparado = textoseparado + caracter + " ";
			
			if (caracter == 'a') {
				cuentaA++;
			} else if (caracter == 'e') {
				cuentaE++;
			} else if (caracter == 'i') {
				cuentaI++;
			} else if (caracter == 'o') {
				cuentaO++;
			} else if (caracter == 'u') {
				cuentaU++;
			}
			
		}
		System.out.println(textoseparado);
		System.out.println("El porcentaje de a es "+((double)cuentaA/longitud*100)+"%" );
		System.out.println("El porcentaje de e es "+((double)cuentaE/longitud*100)+"%" );
		System.out.println("El porcentaje de i es "+((double)cuentaI/longitud*100)+"%" );
		System.out.println("El porcentaje de o es "+((double)cuentaO/longitud*100)+"%" );
		System.out.println("El porcentaje de u es "+((double)cuentaU/longitud*100)+"%" );
		input.close();

	}

}
