package ejercicio05;

import java.util.Scanner;

public class Ejercicio0501 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Programa  (bucle  for)  que  recibe  dos  números  enteros  (un  inicio  y  un  final)  y  después  un 
		//tercer número que indica el salto. El programa debe mostrar por pantalla todos los números 
	//	desde  el  inicio  hasta  el  final  sumando  o  restando  el  salto  (Debe  comprobar  si  el  inicio  es 
	//	mayor o menor al final). Ejemplo1: inicio 1, final 11, salto 2 (1,3,5,7,9,11). Ejemplo2: inicio 
	//	25, final 5, salto 6 (25, 19, 13, 7, 1) 
				
		Scanner entrada = new Scanner(System.in);
		System.out.println("Dame un primer número");
		int n1 = entrada.nextInt();
		System.out.println("Dame el siguiente número");
		int n2 = entrada.nextInt();
		System.out.println("Dame el incremento");
		int n3= entrada.nextInt();
		if (n2>n1) {
			for(int i=n1; i<=n2; i = i+n3) {
				System.out.println(i);
			}
		} else {
			for(int i=n1; i>=n2; i =i-n3) {
				System.out.println(i);
			}
		}
		
				
		entrada.close();

	}

}
