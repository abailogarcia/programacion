package ejercicio20bis;

import java.util.Scanner;

public class Ejercicio20bis {

	public static void main(String[] args) {
		//  Programa  que  me  cuenta  el  número  de  veces  que  aparece  un  carácter  en  una  cadena  de 
		//texto. El carácter se pedirá por teclado, y posteriormente se pedirá una cadena de texto.  
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine();
		System.out.println("Introduce un carácter");
		char caracter = input.nextLine().charAt(0);
		int contador =0;
		int longitud =texto.length();
		for (int i=0; i<longitud; i++) {
			if (texto.charAt(i)==caracter) {
				contador++;
			}
		}
		System.out.println("El carácter aparece "+contador+ " veces");
		
		
		
		input.close();
	}

}
