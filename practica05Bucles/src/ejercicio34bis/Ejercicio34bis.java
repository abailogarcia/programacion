package ejercicio34bis;

import java.util.Scanner;

public class Ejercicio34bis {

	public static void main(String[] args) {
		// Comprobar si un número introducido por teclado es primo. Un número primo es aquel que 
		//solo es divisible entre sí mismo y entre 1. Un número es divisible entre otro si el resto es 0.

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un número");
		int numero = input.nextInt();
		boolean primo=true;
		for (int i=2; i<numero;i++) {
			if(numero%i==0) {
				primo=false;
				break;
			}
					
		}
		if (primo) {
				System.out.println(numero+" es primo");
			}else {
				System.out.println(numero+" no es primo");
			}
		input.close();
		
	}

}
