package ejercicio32bis;

import java.util.Scanner;

public class Ejercicio32bis {

	public static void main(String[] args) {
		// Crea  una  aplicación  que dibuje  una  escalera  de números,  siendo  cada  línea 
		//números empezando en uno y acabando en el número de la línea. Este es un 
		//ejemplo, si introducimos un 5 como altura: 

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una altura");
		int altura = input.nextInt();
		
		for (int i=1; i<=altura; i++) {
			for (int j=1; j<=i; j++) {
				System.out.print(j);
			}
			System.out.println("");
		}
					
		
		input.close();
		
	}

}
