package ejercicio37bis;

public class Ejercicio37bis {

	public static void main(String[] args) {
		// Mostrar los números perfectos entre 1 y 1000.
		
		for (int i =1; i<=1000;i++) {
			int suma =0;
			for (int j=1; j<i;j++) {
				
				if(i%j==0) {
					suma +=j;
				}
			}
			if(suma == i) {
				System.out.println(i);
			}
		}
		
	}

}
