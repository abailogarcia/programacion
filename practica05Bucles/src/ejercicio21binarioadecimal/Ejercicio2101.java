package ejercicio21binarioadecimal;

import java.util.Scanner;

public class Ejercicio2101 {

	public static void main(String[] args) {
		// Programa que pide un número binario de 8 cifras y muestra su conversión a sistema decimal. 
		//Debemos  crear  un  algoritmo  que  haga  eso,  sin  usar  clases  nuevas.  Solo  tenemos  que 
		//recordar  las  operaciones  que  debo  hacer  para  pasar  de  binario  a  decimal,  y  encontrar  el 
		//patrón que se repite. Después representar esa repetición mediante un bucle. 
		
		Scanner input =new Scanner(System.in);
		System.out.println("Dame un número binario de 8 digítos");
		String numero = input.nextLine();
		int longitud = numero.length();
		int decimal =0;
		if (longitud !=8) {
			System.out.println("No es un número correcto");
		} else {
			
			for (int i =7; i>=0; i--) {
				int base = (int)numero.charAt(i);
				if (base == 48) {
					base =0;
				} else {
					base =1;
				}
				decimal = decimal + (base*(int)(Math.pow(2,(longitud -i -1))));
				
				
			}
				
		}
			
		System.out.println("El binario " + numero +" equivale, en decimal, a " + decimal);	
			
			
		input.close();	
		}
		
		
		
		
		
		


}
