package ejercicio24bis;

import java.util.Scanner;

public class Ejercicio24bis {

	public static void main(String[] args) {
		//  Programa que pida el largo y el ancho (valores enteros) y dibuje un rectángulo, con tantas 
		//‘X’ por alto y por ancho como se han introducido por teclado. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el alto");
		int alto = input.nextInt();
		System.out.println("Introduce el ancho");
		int ancho = input.nextInt();
		for (int i=1;i<=alto;i++) {
			for (int j=1;j<=ancho;j++) {
				System.out.print("X ");
			}
			System.out.println("");
		}
				
		input.close();

	}

}
