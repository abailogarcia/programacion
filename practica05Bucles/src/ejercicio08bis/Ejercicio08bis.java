package ejercicio08bis;

public class Ejercicio08bis {

	public static void main(String[] args) {
		// Programa que muestra el abecedario en castellano en mayúsculas.
		
		for (int i= 65; i<=90;i++) {
			char caracter=(char)i;
			System.out.println(caracter);
		}

	}

}
