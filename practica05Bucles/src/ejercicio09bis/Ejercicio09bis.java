package ejercicio09bis;

public class Ejercicio09bis {

	public static void main(String[] args) {
		// Generar un número entero aleatorio entre 0 y 100 ayudándome del método Math.random(). 
		//Posteriormente muestro por pantalla todos los números desde el número aleatorio hasta el 
		//-100, de 7 en 7. 

		double aleatorio = Math.random()*101;
		int entero = (int)aleatorio;
		for (int i =entero; i>= -100; i-=7) {
			System.out.println(i);
		}
		
		
	}

}
