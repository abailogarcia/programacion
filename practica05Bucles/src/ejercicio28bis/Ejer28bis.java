package ejercicio28bis;

import java.util.Scanner;

public class Ejer28bis {

	public static void main(String[] args) {
		// Programa que pida número enteros, hasta introducir el número 0. En ese momento indicar 
		//cuál es el mayor, el menor y la cantidad de números leída, sin contar el 0. 

		Scanner input =new Scanner(System.in);
		int numero;
		int mayor =0;
		int menor = Integer.MAX_VALUE;
		int contador=0;
		do {
			System.out.println("Introduzca un entero");
			numero = input.nextInt();
			if (numero>mayor) {
				mayor =numero;
				
			}
			if (numero <menor && numero !=0) {
				menor =numero;
			}
			contador ++;
			
		} while (numero !=0);
		System.out.println("El máximo es " + mayor);
		System.out.println("El mínimo es "+ menor);
		System.out.println("Has introducido " + (contador-1));
		
		input.close();
	}

}
