package ejercicio29contarvocalesyporcentaje;

import java.util.Scanner;

public class Ejercicio2901 {

	public static void main(String[] args) {
		// Programa  que  pida  cadenas  de  texto  hasta  introducir  la  cadena  de  texto  “fin”. 
		//Posteriormente me dirá la cantidad de vocales (mayúsculas o minúsculas) y el porcentaje de 
		//vocales respecto al total de caracteres. Además me mostrará la cadena resultante de unir 
		//todas las cadenas separadas por un espacio. 
		Scanner input =new Scanner(System.in);
			//cadena
			String cadena;
			//contador para las vocales
			int contadorVocales=0;
			//acumulador para contar todos los caracteres
			int contadorTotalCaracteres=0;
			//cadenaTotal
			String cadenaTotal="";
		do {
			System.out.println("Introduzca un texto o fin para terminar");
			//transformamos a minúsculas todo
			cadena = input.nextLine().toLowerCase();
			//acumulamos caracteres totales
			contadorTotalCaracteres = contadorTotalCaracteres + cadena.length();
			int caracter=0;
			if (!cadena.equals("fin")) {
				for (int i =0; i<(cadena.length()); i++) {
					caracter = cadena.charAt(i);
					//yo lo he pasado a ascii pero sin convertir sería:
					//char caracter=cadena.charAt(i);
					//if (caracter =='a'|| caracter =='b'||)
					
					if (caracter==97 || caracter==101 || caracter==105 || caracter==111 || caracter==117 ) {
						contadorVocales ++;
					}
				}	
				cadenaTotal = cadenaTotal + " " + cadena;
			}
			
		} while (!cadena.equals("fin"));
		
		
		System.out.println("Cantidad de vocales "+contadorVocales);
		System.out.println("Porcentaje de vocales " +((double)contadorVocales/contadorTotalCaracteres)*100 + "%");
		System.out.println("Cadena total "+cadenaTotal);
		
		
		input.close();
		
		
	}

}
