package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		// Pedir 10 números reales por teclado y después escribir la suma total.
		Scanner input = new Scanner(System.in);

		double suma = 0;
		for (int i = 0; i < 10; i++) {
			
			System.out.println("Dame un número real");
			double n = input.nextDouble();
			suma = suma + n;

		}
		
		System.out.println("La suma de todos los números es " + suma);
		input.close();

	}

}
