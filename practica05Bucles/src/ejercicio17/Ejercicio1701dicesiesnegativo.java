package ejercicio17;

import java.util.Scanner;

public class Ejercicio1701dicesiesnegativo {

	public static void main(String[] args) {
		// Pedir 10 números, y decir al final si se ha introducido alguno negativo. 
		
		Scanner input =new Scanner(System.in);
		int contador =0;
		//int numero =0;
		for (int i=0; i<10; i++) {
			System.out.println("Introduce un número");
			int numero = input.nextInt();
			if (numero <0) {
				contador++;
			}
								
		}
		if (contador >0) {
		System.out.println("Has metido algún número negativo");
		} else {
			System.out.println("No has metido ningún número negativo");
		}
		
		input.close();

	}

}
