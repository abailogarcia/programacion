package ejercicio01numerode1aX;

import java.util.Scanner;

public class ejercicio0101 {

	public static void main(String[] args) {
		// 
		// Programa que muestre los números del 1 a un número mayor introducido por
		// teclado
//utilizando la instrucción for. Realizar el  ejercicio mostrando los números cada  uno en una 
//línea.  Y  después  mostrándolos  en  la  misma  línea  separados  por  un  espacio.  Mostrar  un 
//mensaje que indique el final del programa
		Scanner entrada = new Scanner(System.in);
		System.out.println("Dame una cantidad");
		int cantidad = entrada.nextInt();

		for (int i = 1; i <= cantidad; i++) {
			System.out.println("El número es " + i);

		}
		System.out.print("Los números son ");
		for (int i = 1; i <= cantidad; i++) {
			System.out.print(i + " ");

		}
		System.out.println("");
		System.out.println("Se acabó");

		entrada.close();

	}

}
