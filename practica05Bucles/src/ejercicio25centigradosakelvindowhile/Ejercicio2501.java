package ejercicio25centigradosakelvindowhile;

import java.util.Scanner;

public class Ejercicio2501 {

	public static void main(String[] args) {
		// Pasar  de  grados  centígrados  a  grados  kelvin.  El  proceso  de  leer  grados  centígrados  y 
		//convertirlos se debe repetir mientras que se responda ‘S’ a la pregunta: ¿Repetir proceso? 
			//	(S/N). (Conversión: ºK = ºC + 273) 
		
		Scanner input = new Scanner(System.in);
		
		char respuesta;
		double centigrados;
		
		do {
			System.out.println("Introduce grados centígrados");
			centigrados = input.nextDouble();
			//acabo de leer un número y antes de leer el carácter tengo que limpiar el búfer
			input.nextLine();
			System.out.println("kelvin " + (centigrados+273));
			System.out.println("Repetimos (S/N)");
			respuesta =input.nextLine().charAt(0);
			
		} while (respuesta =='S' || respuesta =='s');
		
		
		input.close();
		
		
	}

}
