package ejercicio22binarioadecimal;

import java.util.Scanner;

public class Ejercicio2201 {

	public static void main(String[] args) {
		// Mismo ejercicio anterior pero que permita convertir un número binario de
		// cualquier cantidad de cifras. Usar tipo long.

		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número binario de 8 digítos");
		String numero = input.nextLine();
		int longitud = numero.length();
		int decimal = 0;

		for (int i = (longitud - 1); i >= 0; i--) {
			int base = (int) numero.charAt(i);
			if (base == 48) {
				base = 0;
			} else {
				base = 1;
			}
			decimal = decimal + (base * (int) (Math.pow(2, (longitud - i - 1))));

		}

		System.out.println("El binario " + numero + " equivale, en decimal, a " + decimal);

		input.close();
	}

}