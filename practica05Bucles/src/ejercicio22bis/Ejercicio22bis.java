package ejercicio22bis;

import java.util.Scanner;

public class Ejercicio22bis {

	public static void main(String[] args) {
		// Programa que pide un número binario de 8 cifras y muestra su conversión a
		// sistema decimal.
		// Debemos crear un algoritmo que haga eso, sin usar clases nuevas. Solo tenemos
		// que
		// recordar las operaciones que debo hacer para pasar de binario a decimal, y
		// encontrar el
		// patrón que se repite. Después representar esa repetición mediante un bucle.

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un binario");
		String numero = input.nextLine();
		int longitud = numero.length();
		int decimal = 0;

		for (int i = (longitud - 1); i >= 0; i--) {
			int base = (int) numero.charAt(i);
			if (base == 48) {
				base = 0;
			} else {
				base = 1;
			}
			decimal = base + (base * (int) (Math.pow(2, (longitud - i - 1))));
		}
		System.out.println("En decimal es " + decimal);
		input.close();

	}

}
