package ejercicio03;

import java.util.Scanner;

public class Ejercicio0301 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Programa que muestre los números, descendiendo de uno en uno desde un número
		// introducido por teclado mayor al 1 hasta el número 1 utilizando la
		// instrucción do-while.
		// Realizar el ejercicio mostrando los números cada uno en una línea. Y después
		// mostrándolos
		// en la misma línea separados por un espacio. Mostrar un mensaje que indique el
		// final del
		// programa.

		Scanner entrada = new Scanner(System.in);
		System.out.println("Dame una cantidad (mayor que 1)");
		int cantidad = entrada.nextInt();
		int cantidad2 = cantidad;
		do {
			System.out.println(cantidad);
			cantidad--;
		} while (cantidad > 0);

		cantidad = cantidad2;
		do {
			System.out.print(cantidad + " ");
			cantidad--;
		} while (cantidad > 0);

		System.out.println("");
		System.out.println("Se acabó");

		entrada.close();
	}

}
