package ejercicio34tris;

import java.util.Scanner;

public class Ejercicio34tris {

	public static void main(String[] args) {
		// Realizar un programa que nos pida un número, y nos diga cuantos números hay
				// entre 1 y n
				// que son primos.
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un número");
		int numero = input.nextInt();
		int primos=0;
		for (int i= 1; i<=numero;i++) {
			int contador=0;
			for (int j=2; j<i;j++) {
				if(i%j==0) {
					
					contador++;
				}
				
			}
			if (contador==0) {
				System.out.println(i);
				primos++;
			}
		}
		System.out.println("Hay "+ primos+ " números primos");
		
		input.close();

	}

}
