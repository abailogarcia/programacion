package ejercicio34primos;

import java.util.Scanner;

public class Ejercicio34primos {

	public static void main(String[] args) {
		// Comprobar si un número introducido por teclado es primo. Un número primo es aquel que 
		//solo es divisible entre sí mismo y entre 1. Un número es divisible entre otro si el resto es 0.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un número y te digo si es primo");
		int numero = input.nextInt();
		boolean esPrimo=true;
		for (int i =2; i<numero;i++) {
			if ((numero % i)==0) {
				esPrimo=false;
				break;
			}
			
		}
		if (esPrimo) {
			System.out.println("El número "+ numero +" es primo");
		} else {
			System.out.println("El número "+ numero +" no es primo");
		}
		
		input.close();

	}

}
