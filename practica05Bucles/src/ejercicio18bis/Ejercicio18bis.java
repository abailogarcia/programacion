package ejercicio18bis;

import java.util.Scanner;

public class Ejercicio18bis {

	public static void main(String[] args) {
		// Pedir un número entero que representa el tamaño del lado. Dibujar y rellenar un cuadrado 
		//con el carácter ‘X’, con tantas filas y columnas como se ha indicado por teclado. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introuce el lado del cuadrado");
		int lado = input.nextInt();
		for (int i =1; i<=lado; i++) {
			for (int j=1; j<=lado; j++) {
				System.out.print("X ");
				
			}
			System.out.println("");
			
		}
		
		input.close();

	}

}
