package ejercicio06contarcifrasdeunnumero;

import java.util.Scanner;

public class Ejercicio0601 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	//	Mostrar  el  número  de  cifras  de  un  número  introducido  por  teclado.  Los  datos  deben  ser 
		//tratados  como  números  enteros,  no  cómo  String  (Se  debe  resolver  sin  los  métodos  de  la 
		//clase String). 
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce un número");
		int cantidad = entrada.nextInt();
		int contador=1;               //se podría hacer int contador=0;
		while(cantidad >10) {		  //while(cantidad>0)
			cantidad = cantidad/10;
			contador++;
		}
		
		
		
		System.out.println("El número tiene " + contador + " cifras");
		entrada.close();
		
	}

}
