package ejercicio18dibujarcuadrado;

import java.util.Scanner;

public class Ejercicio1801dibujarcuadrado {

	public static void main(String[] args) {
		// Pedir un número entero que representa el tamaño del lado. Dibujar y rellenar
		// un cuadrado
		// con el carácter ‘X’, con tantas filas y columnas como se ha indicado por
		// teclado.

		Scanner input = new Scanner(System.in);
		System.out.println("Dime el número de lados del cuadrado");
		int lados = input.nextInt();

		for (int i = 1; i <= lados; i++) {

			for (int j = 0; j < lados; j++) {

				System.out.print("X ");
			}
			System.out.println();
		}

		input.close();

	}

}
