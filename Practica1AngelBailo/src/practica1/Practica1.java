package practica1;

import java.util.Scanner;

public class Practica1 {

	public static void main(String[] args) {
		// examen
		Scanner input = new Scanner(System.in);
		boolean salir = false;
	    int opcion;
	        
	       while(!salir){
	            
	    	   System.out.println("*********************************");
	           System.out.println("1- Calcular importe compra");
	           System.out.println("2- Ordena jugadores");
	           System.out.println("3- Frecuencia vocales");
	           System.out.println("4- Dinero en cuenta ");
	           System.out.println("5- Salir");
	           System.out.println("*********************************");
	           System.out.println("");
	           System.out.println("Introduzca el número de opción:");
	           opcion = input.nextInt();
	           switch(opcion){
	           case 1:
	        //Esta opción pedirá introducir una cantidad por teclado correspondiente al 
	       //total de una compra OnLine. La compra tiene un recargo correspondiente a 4.95€ por el envío. 
	        //El programa solicitará un código de descuento. Si el código es FDTY se realizará un descuento 
	   //del  15%  por  la  compra.  Si  el  código  es  HTRG  se  eliminarán  los  gastos  de  envío.  Visualizar  el 
	         //total a pagar por la compra y los descuentos aplicados.
                   System.out.println("Introduce importe compra");
                   double importe = input.nextDouble();
                   input.nextLine();
                   System.out.println("Introduce el código de descuento");
                   String codigo = input.nextLine().toUpperCase();
                   double total =0;
                   double descuento =0;
                   if (codigo.equals("FDTY")) {
                	   total = (importe+4.95)*0.85;
                	   descuento = (importe+4.95)*0.15;
                   } else if (codigo.equals("HTRG")) {
                	   total = importe;
                	   descuento = 4.95;
                   } else {
                	   total = importe +4.95;
                	   descuento =0;
                   }
                   System.out.println("El total de la compra es "+ total);
                   System.out.println("El descuento aplicado es "+ descuento);
                   
                   break;
               case 2:
           //Esta opción pedirá por separado el nombre, el apodo y la puntuación de 3 
           // jugadores. El programa mostrará los datos de los 3 jugadores en una línea cada uno, 
            //  ordenados  por  apodo  de  menor  a  mayor  con  el  siguiente  formato.  El  apodo  se  mostrará  en 
            	// mayúsculas. 
            	  // APODO – Nombre (Puntuación)  
            	  // Ejemplo: 
            	   // CICLOPE – Carlos (33) 
            	   // JANO - Juan Carlos (123) 
            	    //VIRUS – Raquel (78) 
            	   //La ordenación no tendrá consideración entre mayúsculas- minúsculas (usar método de la clase 
            	   //String para comparar). 
            	   input.nextLine();
                   System.out.println("Introduce nombre de jugador 1");
                   String nombre1= input.nextLine();
                   System.out.println("Introduce apodo de jugardor 1");
                   String apodo1= input.nextLine().toUpperCase();
                   System.out.println("Introduce puntuación de jugador 1");
                   String punto1 = input.nextLine();
                   System.out.println("Introduce nombre de jugador 2");
                   String nombre2= input.nextLine();
                   System.out.println("Introduce apodo de jugardor 2");
                   String apodo2= input.nextLine().toUpperCase();
                   System.out.println("Introduce puntuación de jugador 2");
                   String punto2 = input.nextLine();
                   System.out.println("Introduce nombre de jugador 3");
                   String nombre3= input.nextLine();
                   System.out.println("Introduce apodo de jugardor 3");
                   String apodo3= input.nextLine().toUpperCase();
                   System.out.println("Introduce puntuación de jugador 3");
                   String punto3 = input.nextLine();
                 //  int valor= apodo1.compareTo(apodo2);
                 //  System.out.println(valor);
                   if (apodo1.compareTo(apodo2)<0) {
                	   if ( apodo1.compareTo(apodo3)<0) {
                		   System.out.println(apodo1+ " - " + nombre1+ " ("+punto1+")");
                		   		if(apodo2.compareTo(apodo3)<0) {
                		   				System.out.println(apodo2+ " - " + nombre2+ " ("+punto2+")");
                		   				System.out.println(apodo3+ " - " + nombre3+ " ("+punto3+")");
                		   		} else {
                		   				System.out.println(apodo3+ " - " + nombre3+ " ("+punto3+")");
                		   				System.out.println(apodo2+ " - " + nombre2+ " ("+punto2+")");
                		   		}
                	   		} else {
                			   System.out.println(apodo3+ " - " + nombre3+ " ("+punto3+")");
                			   System.out.println(apodo1+ " - " + nombre1+ " ("+punto1+")");
                			   System.out.println(apodo2+ " - " + nombre2+ " ("+punto2+")");
                		   }
                   		} else if (apodo2.compareTo(apodo3)<0){
                   			System.out.println(apodo2+ " - " + nombre2+ " ("+punto2+")");
                   			if (apodo1.compareTo(apodo3)<0) {
                   				System.out.println(apodo1+ " - " + nombre1+ " ("+punto1+")");
                   				System.out.println(apodo3+ " - " + nombre3+ " ("+punto3+")");
                   				}else {
                   					System.out.println(apodo3+ " - " + nombre3+ " ("+punto3+")");
                   					System.out.println(apodo1+ " - " + nombre1+ " ("+punto1+")");
                   					}
                   			} else {
                   				System.out.println(apodo3+ " - " + nombre3+ " ("+punto3+")");
                   				System.out.println(apodo2+ " - " + nombre2+ " ("+punto2+")");
                   				System.out.println(apodo1+ " - " + nombre1+ " ("+punto1+")");
                   			}
                   
                	   
                   break;
                case 3:
                	//Esta  opción  pedirá  una  frase  con  varias  palabras.  El  programa  mostrará  la 
                	//frecuencia de aparición de las vocales del siguiente modo: 
                	//             a    8     ******** 
                	//             e    6     ****** 
                	//             i     0      
                	//             o    3     *** 
                	//             u    1     *
                   System.out.println("Introduzca una frase");
                   input.nextLine();
                   String texto = input.nextLine().toLowerCase();
                   int longitud = texto.length();
                   int cuentaA=0;
                   String asteriscoA="";
                   int cuentaE=0;
                   String asteriscoE="";
                   int cuentaI=0;
                   String asteriscoI="";
                   int cuentaO=0;
                   String asteriscoO="";
                   int cuentaU=0;
                   String asteriscoU="";
                   for (int i = 0; i< longitud; i++) {
                	   if (texto.charAt(i)=='a') {
                		   cuentaA++;
                		   asteriscoA=asteriscoA+"*";
                	   }
                	   if (texto.charAt(i)=='e') {
                		   cuentaE++;
                		   asteriscoE=asteriscoE+"*";
                	   }
                	   if (texto.charAt(i)=='i') {
                		   cuentaI++;
                		   asteriscoI=asteriscoI+"*";
                	   }
                	   if (texto.charAt(i)=='o') {
                		   cuentaO++;
                		   asteriscoO=asteriscoO+"*";
                	   }
                	   if (texto.charAt(i)=='u') {
                		   cuentaU++;
                		   asteriscoU=asteriscoU+"*";
                	   }
                	   
                   }
                   System.out.println(" a    "+ cuentaA+ "    "+ asteriscoA);
                   System.out.println(" e    "+ cuentaE+ "    "+ asteriscoE);
                   System.out.println(" i    "+ cuentaI+ "    "+ asteriscoI);
                   System.out.println(" o    "+ cuentaO+ "    "+ asteriscoO);
                   System.out.println(" u    "+ cuentaU+ "    "+ asteriscoU);
                   break;
                case 4:
             // Programa  para  controlar  el  saldo  de  una  cuenta.  El  programa  inicialmente 
            //nos  pedirá  el  saldo  de  la  cuenta.  Posteriormente  nos  pide  constantemente  la  cantidad  de 
            //Dinero  que  ingresar  a  la  cuenta  (positivo)  o  que  retiramos  (negativo).  Después  de  cada 
           //cantidad  debe  mostrar  el  saldo  restante  de  la  cuenta.  El  programa  deja  de  pedir  cantidades 
           //cuando no queda dinero en cuenta.
                	System.out.println("Introduzca el saldo de la cuenta");
                	double saldo = input.nextDouble();
                	do {
                	System.out.println("Introduzca el ingreso(+) o el pago(-)");
                	double movimiento = input.nextDouble();
                	saldo = saldo + movimiento;
                	System.out.println("El nuevo saldo es " + saldo);
                	
                	} while (saldo!=0);
                	break;
                case 5:
                   salir=true;
                   System.out.println("Fin de programa");
                   break;
                default:
                   System.out.println("Solo números entre 1 y 5");
           }
		
	   }
	 input.close();      
	}
}
