package ejer02intadouble;

import java.util.Scanner;

public class Ejer02intadouble {

	public static void main(String[] args) {
		// Programa  que  pide  un  primer  número  correspondiente  a  la  cantidad  de 
		//hombres, un segundo número correspondiente a la cantidad de mujeres y muestra los 
		//siguientes mensajes: 
		//La cantidad de hombres es:  
		//La cantidad de mujeres es:  
		//El total es:  
		//El porcentaje de hombres es:  
		//El porcentaje de mujeres es: 
		
		Scanner entrada = new Scanner(System.in);
		
		int hombres;
		int mujeres;
		double total;
		
		System.out.println("Dame la cantidad de hombres");
		hombres = entrada.nextInt();
		System.out.println("Dame la cantidad de mujeres");
		mujeres = entrada.nextInt();
		
		total = mujeres + hombres;
		
		System.out.println("La cantidad de hombre es " +hombres);
		System.out.println("La cantidad de mujeres es " +mujeres);
		System.out.println("El total es " +total);
		System.out.println("El porcentaje de hombres es " +(double)(hombres/total*100)+"%");
		System.out.println("El porcentaje de mujeres es " +(double)(mujeres/total*100)+"%");		
		entrada.close();

	}

}
