package ejer01mediaponderada;

import java.util.Scanner;

public class Ejer01mediaponderada {

	public static void main(String[] args) {
		// Programa que pide  tres notas y calcula su promedio, sabiendo que la 1ª y 
		//2ª tienen un 15% de peso cada una y la 3ª un 70% de peso 
		
		Scanner input = new Scanner(System.in);
		System.out.println("Dame una nota");
		double n1 = input.nextDouble();
		System.out.println("Dame otra nota");
		double n2 = input.nextDouble();
		System.out.println("Dame otra nota");
		double n3 = input.nextDouble();
		
		System.out.println("La media es " + (n1*0.15 + n2*0.15+n3*0.7));
		
		input.close();

	}

}
