package ejer03comprobarstring;

import java.util.Scanner;

public class Ejer03comprobarstring {
	
	static final String NOMBRE = "juan";
	static final String PASS = "1234";

	public static void main(String[] args) {
		// Realizar un programa que permite validar un código de descuento 
		//solicitando  el  nombre  de  usuario  y  un  código.  Se  creará  un  String  para  el  usuario,  otro  String 
		//para el código de descuento y se les dará valores. El programa comprobará que el usuario y los 
		//códigos son iguales a los creados (coincidencia completa: mayúsculas/minúsculas). 
		//En caso de ser introducir los datos correctos (ambos, usuario y código), se mostrará el mensaje 
		//por  pantalla  “El  descuento  es  válido”. En caso contrario, mostrará el mensaje “Nombre de 
		//usuario o descuento Incorrecto”.

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el usuario");
		String name = input.nextLine();
		System.out.println("Introduce el password");
		String clave =input.nextLine();
		
		if (name.equals(NOMBRE)&&clave.equals(PASS)) {
			System.out.println("El descuento es válido");
		} else {
			System.out.println("Nombre de usuario o descuento Incorrecto");
		}
		
		
		
		input.close();
		
	}

}
