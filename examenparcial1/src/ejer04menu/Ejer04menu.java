package ejer04menu;

import java.util.Scanner;

public class Ejer04menu {

	public static void main(String[] args) {
		// Crear un programa que muestre un menú y pida seleccionar una opción 
		//(número entero). El menú tendrá 3 opciones:  
			// 1 – Comprobar número 
			 //2 – Comprobar carácter 
			 //3 - Salir  
			//a) (1  pto)  Opción  1:  La  opción  1  pide  un  número  entero.  Si  el  número  introducido  es 
			//negativo, lo indica y no hace nada más. Si es positivo el programa indica si es múltiplo de 5 
			//o no. 
			//b) (1  pto)  Opción  2:  La  opción  2  pide  una  cadena.  Si  he  introducido  más  de  un  carácter  lo 
			//indica por mensaje y no hace nada más. Si he introducido un solo carácter, indica si es una 
			//letra, un número o un signo. 
			//c) (1  pto)  Usar  un  switch  para  ejecutar  la  opción  pedida.  Si  selecciono  otra  opción  no 
			//contemplada el programa me avisará. El menú se mostrará una sola vez. Si selecciono Salir, 
			//el programa termina. 
		
		Scanner input = new Scanner(System.in);
		System.out.println("1.- Comprobar número");
		System.out.println("2.- Comprobar carácter");
		System.out.println("3.- Salir");
		
		int opcion = input.nextInt();
		
		if (opcion >=1 && opcion <=3) {
			switch (opcion) {
				case 1:
					System.out.println("Introduce un número entero");
					int n1 = input.nextInt();
					if (n1<0) {
						System.out.println("El número es negativo");
					} else if (n1%5==0) {
						System.out.println("El número " + n1 + " es múltiplo de 5");
					} else {
						System.out.println("El número " + n1 + " no es múltiplo de 5");
					}
					break;
				case 2:
					input.nextLine();
					System.out.println("Introduce un texto");
					String texto = input.nextLine().toLowerCase();
					int longitud = texto.length();
					int caracter = (int)texto.charAt(0);
					if (longitud == 1) {
						if (caracter>=97 && caracter<=122) {
							System.out.println("El carácter es una letra");
						} else if (caracter>=48 && caracter<=57) {
							System.out.println("El carácter es un número");
						} else {
							System.out.println("El carácter es un signo");
						}
					} else {
						System.out.println("El texto tiene más de un carácter");					
					}
					break;
				case 3:
					System.out.println("Programa terminado");
			}
			
		}else {
			System.out.println("La opción seleccionada no es correcta");
			System.out.println("Programa terminado");
		}
		
		
		
		input.close();

	}

}
