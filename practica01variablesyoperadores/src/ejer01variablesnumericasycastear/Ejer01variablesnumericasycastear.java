package ejer01variablesnumericasycastear;

public class Ejer01variablesnumericasycastear {

	public static void main(String[] args) {
		// Programa java que realice lo siguiente: declarar una variable de tipo int, una variable de tipo 
		//double  y  una  variable  de tipo  char  y  asigna  a  cada  una  un valor.  A  continuación,  muestra  por 
		//pantalla: El valor de cada variable, la suma de int + double, la diferencia de double - int, el valor 
		//numérico correspondiente al carácter que contiene la variable char (conversión cast).

		int entero = 45;
		double doble = 4.6;
		char caracter = 'a';
		
		System.out.println(entero);
		System.out.println(doble);
		System.out.println(caracter);
		System.out.println("la suma es "+(entero+ doble));
		System.out.println("la resta es " +(doble -entero));
		
		System.out.println("el valor numérico es " + (int)caracter);
		
	}

}
