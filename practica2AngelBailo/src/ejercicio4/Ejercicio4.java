package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		// PUnta de flecha
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce la anchura de la flecha");
		int ancho=input.nextInt();
		for(int i=1;i<=ancho;i++) {
			for (int j=1;j<=i;j++) {
			System.out.print("*");
			}
			System.out.println("");
		}
		for(int k=ancho;k>=1;k--) {
			for (int l=k;l>=1;l--) {
				System.out.print("*");
			}
			System.out.println("");
		}
		
		input.close();

	}

}
