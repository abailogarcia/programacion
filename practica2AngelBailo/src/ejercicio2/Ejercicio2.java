package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// Crear un programa que me pide un número positivo (veces). Después me
		//pide y lee tantos números como me indica ese número inicial (veces). (1 pto).
		//Por cada número leído el programa me dice si es capicúa o no. (1 pto)
		//Al terminar de leer todos los números (cuando para el bucle) me calcula la media de todos
		//ellos. (1 pto
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el número de veces");
		int veces = input.nextInt();
		double suma=0;
		for (int i=1; i<=veces;i++) {
			System.out.println("Introduce un número");
			int numero=input.nextInt();
			
			String cadena = numero+""; // Lo convierto en cadena para ver si es capicúa
			int longitud = cadena.length();
			int contador=0;	
			suma+=numero;
			for (int j=0; j<longitud; j++) {
				char caracter = cadena.charAt(j);
				char caracterSimetrico = cadena.charAt((longitud-j-1));
				if (caracter==caracterSimetrico) {
					contador++;
				}
			}
			if (contador==longitud) {
				System.out.println("El número es capicúa");
			} else {
				System.out.println("El número no es capicúa");
			}
			
		}

		System.out.println("La media de los números es " +(suma/veces));
		input.close();

	}

}
