package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// Ciclo Formativo: Desarrollo de aplicaciones
		// 1 – contar acentos
		// 2 – contraseña correcta
		// 3 - salir
		// a) (1 pto) Si selecciono otra opción no contemplada el programa me avisará.
		// Debo usar
		// un switch para ejecutar la opción pedida. El menú se repite después de cada
		// opción (1
		// o 2) hasta pulsar 3-salir.
		Scanner input = new Scanner(System.in);
		boolean salir = false;
		int opcion;

		while (!salir) {
			System.out.println("Introduce una opción");
			System.out.println("1 – contar acentos");
			System.out.println("2 – contraseña correcta");
			System.out.println("3 – Salir");

			System.out.println("Escribe una de las opciones");
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				// Opción 1: El programa me pide una cadena y después me indica la cantidad de
				// acentos que tiene, tanto mayúsculas, como minúsculas
				System.out.println("Introduce un texto y te digo cuántos acentos tiene");
				input.nextLine();
				String texto = input.nextLine();
				int contador = 0;
				for (int i = 0; i < texto.length(); i++) {
					char caracter = texto.charAt(i);
					if (caracter == 'á' || caracter == 'é' || caracter == 'í' || caracter == 'ó' || caracter == 'ú'
							|| caracter == 'Á' || caracter == 'É' || caracter == 'Í' || caracter == 'Ó'
							|| caracter == 'Ú') {
						contador++;
					}
				}
				System.out.println("El texto tiene " + contador + " acentos");
				break;
			case 2:
				// Opción 2: El programa me pide una contraseña y muestra un único mensaje
				// indicando solamente si es válida o no. Para que sea válida debe contener al
				// menos 8
				// caracteres, 1 mayúscula, 1 minúscula y 1 digito
				System.out.println("Introduce una contraseña con 8 caraceteres (mayus, min, num)");
				input.nextLine();
				String pass = input.nextLine();
				int longitud = pass.length();
				int cuentaMay = 0;
				int cuentaMin = 0;
				int cuentaNum = 0;
				if (longitud >= 8) {
					for (int j = 0; j < longitud; j++) {
						char caracter2 = pass.charAt(j);
						if (caracter2 >= 'A' && caracter2 <= 'Z') {
							cuentaMay++;
						} else if (caracter2 >= '0' && caracter2 <= '9') {
							cuentaNum++;
						} else if (caracter2 >= 'a' && caracter2 <= 'z') {
							cuentaMin++;
						}
					}
					if (cuentaMay > 0 && cuentaNum > 0 && cuentaMin > 0) {
						System.out.println("La contraseña es correcta");
					} else {
						System.out.println("La contraseña no es correcta");
					}

				} else {
					System.out.println("Debes introducir más de 8 caracteres");
				}
				break;

			case 3:
				salir = true;
				break;
			default:
				System.out.println("Solo números entre 1 y 3");
			}

		}

		input.close();

	}

}
