package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// Programa que me pide una cadena de texto (nos referimos a ella como
		//cadenaFinal). Esa cadena leída será la cadena que parará el bucle. Posteriormente me empieza
//	a pedir cadenas y a leerlas hasta que introduzca la cadena que para el bucle (cadenaFinal). (1	pto)
//	Después de terminar de pedir cadenas (cuando para el bucle) me debe indicar una serie de
//	estadísticas:
//	Cantidad de vocales 0,5
//	Cantidad de mayúsculas 0,5
//	Cantidad de minúsculas 0,5
//	Cantidad de arrobas 0,5
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el texto de parada");
		String parada=input.nextLine();
		String texto="";
		int cuentaVocales=0;
		int cuentaMayusculas=0;
		int cuentaMinusculas=0;
		int cuentaArrobas=0;
		do {
			System.out.println("Introduce un texto");
			texto=input.nextLine();
			String textoMin=texto.toLowerCase(); //lo convierto en minúsculas para contar las vocales
			int longitud=texto.length();
			
			for (int i=0; i<longitud;i++) {
				char caracter= texto.charAt(i);
				char caracterMin=textoMin.charAt(i);
				if(caracterMin=='a' || caracterMin=='e' || caracterMin=='i' || caracterMin=='o' || caracterMin=='u') {
					cuentaVocales++;
				} 
				if (caracter>='A' && caracter<='Z'){
					cuentaMayusculas++;
				} 
				if (caracter>='a' && caracter<='z'){
					cuentaMinusculas++;
				}
				if (caracter=='@'){
					cuentaArrobas++;
				}
			}
			
		}while (!parada.equals(texto));
		System.out.println("El número de vocales es " + cuentaVocales);
		System.out.println("El número de mayúsculas es " + cuentaMayusculas);
		System.out.println("El número de minúsculas es " + cuentaMinusculas);
		System.out.println("El número de arrobas es " + cuentaArrobas);
		
		input.close();
		

	}

}
