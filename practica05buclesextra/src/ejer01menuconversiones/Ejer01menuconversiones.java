package ejer01menuconversiones;

import java.util.Scanner;

public class Ejer01menuconversiones {

	public static void main(String[] args) {
		// El programa al ejecutarse debe mostrar el siguiente menú por pantalla 
		 //Menú de opciones: 
		//1.- Convertir a Binario 
		//2.- Convertir a Hexadecimal 
		//3.- Convertir a Octal 
		//4.- Salir 
		//Después de que aparezcan los mensajes que conforman el menú, el programa esperará a que 
		//introduzcamos  un  número  entero.  Después  de  seleccionar  una  opción  y  ejecutar  el  código 
		//referente a la opción debe volverse a mostrar el menú por pantalla. Solo termina el programa 
		//al introducir 4. 
		
		Scanner input = new Scanner(System.in);
		boolean salir = false;
		int opcion;
		System.out.println("Introduzca un número para convertir");
		int numero = input.nextInt();
		while (!salir) {
			System.out.println("Menú de opciones");
			System.out.println("1.- Convertir a Binario ");
			System.out.println("2.- Convertir a Hexadecimal");
			System.out.println("3.- Convertir a Octal");
			System.out.println("4.- Salir ");
			opcion = input.nextInt();
			
			switch (opcion) {
				case 1:
					//Opción 1 Esta opción pedirá introducir una cantidad por teclado y mostrará la 
					//conversión a Binario usando la clase Integer (Integer.toBinaryString) 
					System.out.println("En binario es " + Integer.toBinaryString(numero));
					break;
				case 2:
					//Opción 2 Esta opción pedirá introducir una cantidad por teclado y mostrará la 
					//conversión a Hexadecimal usando la clase Integer (Integer.toHexString)  
					System.out.println("En hexadecimal es " + Integer.toHexString(numero));
					break;	
				case 3:
					//Opción 3 Esta opción pedirá introducir una cantidad por teclado y mostrará la 
					//conversión a Octal usando la clase Integer (Integer.toOctalString)  
					System.out.println("En octal es " + Integer.toOctalString(numero));
					break;
				case 4:
					salir = true;
					System.out.println("Se acabó");
					break;
				default:
					System.out.println("Sólo números entre 1 y 4");
					
			
			}
			
			
		}
		
		
		
		input.close();

	}

}
