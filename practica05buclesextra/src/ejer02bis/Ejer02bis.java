package ejer02bis;

import java.util.Scanner;

public class Ejer02bis {

	public static void main(String[] args) {
		// El programa al ejecutarse debe mostrar el siguiente menú por pantalla 
		 
				//Menú de opciones: 
				 //1.- Números del 1 al 100 con un while 
				// 2.- Positivos negativos o pares 
				 //3.- Salir 
				//Después de que aparezcan los mensajes que conforman el menú, el programa esperará a que 
				//introduzcamos  un  número  entero.  Después  de  seleccionar  una  opción  y  ejecutar  el  código 
				//referente a la opción debe volverse a mostrar el menú por pantalla. Solo termina el programa 
				//al introducir 3. 
		Scanner input = new Scanner (System.in);
		boolean salir = false;
		int opcion;
		while (!salir) {
			System.out.println("1.- Números del 1 al 100 con un while");
			System.out.println("2.- Positivos negativos o pares ");
			System.out.println("3.- Salir");
			opcion = input.nextInt();
			
			switch (opcion) {
				case 1:
					int contador=0;
					while (contador<100) {
						contador++;
						System.out.println(contador);
					}
					
					break;
				case 2:
				//Opción 2 Esta opción lee números y comprueba si son positivos o negativos y pares o 
					//impares	
					int numero=1;
					while (numero!=0) {
						System.out.println("Introduce un número (0 para terminar)");
						numero=input.nextInt();
						if(numero >0) {
							System.out.println("El número es positivo");
						} else {
							System.out.println("El número es negativo");
						}
						if (numero%2==0) {
							System.out.println("El número es par");
						} else {
							System.out.println("El número es impar");
						}
					}
					
					break;
				case 3: 
					salir=true;
					break;
					
				default:
					System.out.println("Introduce una opción entre 1 y 3");
					break;
			
			
			}
			
			
		}
		
		input.close();
	}

}
