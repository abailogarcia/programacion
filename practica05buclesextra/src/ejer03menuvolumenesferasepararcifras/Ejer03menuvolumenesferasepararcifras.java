package ejer03menuvolumenesferasepararcifras;

import java.util.Scanner;

public class Ejer03menuvolumenesferasepararcifras {
static final double PI = 3.141592;
	public static void main(String[] args) {
		// Menú de opciones: 
	//	1.- Volumen esfera 
		// 2.- Numero de tres cifras con cifras por separado 
		// 3.- Letras minúsculas 
		// 4.- Pares entre dos números
		// 5.- Salir 
		//Después de que aparezcan los mensajes que conforman el menú, el programa esperará a que 
		//introduzcamos  un  número  entero.  Después  de  seleccionar  una  opción  y  ejecutar  el  código 
		//referente a la opción debe volverse a mostrar el menú por pantalla. Solo termina el programa 
		//al introducir 5.
		Scanner input = new Scanner (System.in);
		boolean salir=false;
		int opcion;
		while(!salir) {
			System.out.println("1.- Volumen esfera");
			System.out.println("2.- Numero de tres cifras con cifras por separado");
			System.out.println("3.- Letras minúsculas ");
			System.out.println("4.- Pares entre dos números");
			System.out.println("5.- Salir");
			System.out.println("Selecciona una opción");
			opcion= input.nextInt();
			input.nextLine();
			switch (opcion) {
				case 1:
				//Opción 1 Esta opción calcula el volumen de una esfera 
					System.out.println("Introduce el radio de la esfera");
					double radio=input.nextDouble();
					double volumen = (4/3)*Math.PI*Math.pow(radio, 3);
					System.out.println("El volumen es "+volumen);
					
					break;
				case 2:	
			//Opción 2 Esta opción solicita un numero de tres cifras y las muestra por separado
					System.out.println("Introduce un número de 3 cifras");
					String numero=input.nextLine();
					int longitud = numero.length();
					for (int i=0;i<longitud;i++) {
						System.out.print(numero.charAt(i)+ " ");
					}
						System.out.println("");			
					break;
				case 3:
					//Opción 3 Esta opción solicita dos caracteres y comprueba si los dos son letras 
				//	minúsculas 
					System.out.println("Introduzca dos caracteres");
					String texto=input.nextLine();
					int minuscula=0;
					for (int i=0;i<texto.length();i++) {
						if(texto.charAt(i)>='a' && texto.charAt(i)<='z') {
							minuscula++;
							System.out.println("la letra " + texto.charAt(i) + " es minúscula");
						} else {
							
							System.out.println("la letra " + texto.charAt(i) + " no es minúscula");
						}
					} 
					System.out.println("Hay " + minuscula + " minúsculas");
					break;
				case 4:
					//Opción 4 Esta opción solicita dos números y muestra los números pares entre ellos
					System.out.println("Introduzca el primer número");
					int n1=input.nextInt();
					System.out.println("Introduzca el segundo número");
					int n2=input.nextInt();
					int max=0;
					int min=0;
					max=n1>n2?n1:n2;
					min=n1>n2?n2:n1;
					//if( n1>n2) {
						//max=n1;
						//min=n2;
				//	} else {
					//	max=n2;
					//	min=n1;
					//}
					for (int i=min;i<=max;i++) {
						if(i%2==0) {
							System.out.println(i);
						}
					}
					
					break;
				case 5:
					salir =true;
					input.close();
					System.out.println("Se acabó");
					break;
				default:
					System.out.println("Introduce un número entre 1 y 5");
			}
				
						
		}
			
	}

}
