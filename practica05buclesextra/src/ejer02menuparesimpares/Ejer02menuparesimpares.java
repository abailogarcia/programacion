package ejer02menuparesimpares;

import java.util.Scanner;

public class Ejer02menuparesimpares {

	public static void main(String[] args) {
		// El programa al ejecutarse debe mostrar el siguiente menú por pantalla 
		 
		//Menú de opciones: 
		 //1.- Números del 1 al 100 con un while 
		// 2.- Positivos negativos o pares 
		 //3.- Salir 
		//Después de que aparezcan los mensajes que conforman el menú, el programa esperará a que 
		//introduzcamos  un  número  entero.  Después  de  seleccionar  una  opción  y  ejecutar  el  código 
		//referente a la opción debe volverse a mostrar el menú por pantalla. Solo termina el programa 
		//al introducir 3. 
		Scanner input = new Scanner(System.in);
		boolean salir =false;
		int opcion;
		
		while (!salir) {
			System.out.println("Menú de opciones:");
			System.out.println("1.- Números del 1 al 100 con un while ");
			System.out.println("2.- Positivos negativos o pares ");
			System.out.println("3.- Salir");		
			opcion=input.nextInt();
			switch (opcion) {
				case 1:
		//Opción 1 Esta opción muestra los números del 1 al 100 mediante un while 
					//for (int i=1;i<=100;i++) {
					//	System.out.println(i);
					//}
					int contador =0;
					while (contador <100) {
						contador++;
						System.out.println(contador);
					}
					break;
				case 2:
		//Opción 2 Esta opción lee números y comprueba si son positivos o negativos y pares o impares 	
					boolean seleccion=false;
					while (!seleccion) {
						System.out.println("Introduce un número (0 para terminar)");
						int numero = input.nextInt();
						if (numero!=0) {
							if (numero>0) {
								System.out.println("El número "+numero+" es positivo");
							} else {
								System.out.println("El número "+numero+" es negativo");
								}
							if (numero%2==0) {
								System.out.println("El número "+numero+" es par");
								} else {
								System.out.println("El número "+numero+" es impar");
								}
						} else {
							seleccion=true;
							}
					}
					break;
				case 3:
					salir=true;
					System.out.println("Se acabó");
					break;
				default:
					System.out.println("Introduce una opción entre 1 y 3");
			}
			
						
		}
		
		input.close();
	

	}

}
