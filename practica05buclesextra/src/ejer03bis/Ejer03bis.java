package ejer03bis;

import java.util.Scanner;

public class Ejer03bis {

	public static void main(String[] args) {
		// Menú de opciones: 
		//	1.- Volumen esfera 
			// 2.- Numero de tres cifras con cifras por separado 
			// 3.- Letras minúsculas 
			// 4.- Pares entre dos números
			// 5.- Salir 
			//Después de que aparezcan los mensajes que conforman el menú, el programa esperará a que 
			//introduzcamos  un  número  entero.  Después  de  seleccionar  una  opción  y  ejecutar  el  código 
			//referente a la opción debe volverse a mostrar el menú por pantalla. Solo termina el programa 
			//al introducir 5.
		Scanner input =new Scanner(System.in);
		boolean salir=false;
		int opcion;
		while (!salir) {
			System.out.println("___________________________________________________");
			System.out.println("1.- Volumen esfera                                 |");
			System.out.println("2.- Numero de tres cifras con cifras por separado  |");
			System.out.println("3.- Letras minúsculas                              |");
			System.out.println("4.- Pares entre dos números                        |");
			System.out.println("5.- Salir                                          |");
			System.out.println("___________________________________________________|");
			opcion = input.nextInt();
			
			switch (opcion) {
				case 1:
					System.out.println("Introduce el radio");
					int radio = input.nextInt();
					double volumen = (4/3)*Math.PI*Math.pow(radio, 3);
					System.out.println("El volumen es "+volumen);
					break;
			
				case 2:
					//Opción 2 Esta opción solicita un numero de tres cifras y las muestra por separado
					System.out.println("Introduce un número de 3 cifras");
					int numero = input.nextInt();
					System.out.println(numero/100%10);
					System.out.println(numero/10%10);
					System.out.println(numero%10);
					break;
				case 3:
					//Opción 3 Esta opción solicita dos caracteres y comprueba si los dos son letras 
					//minúsculas
					System.out.println("Introduce 2 caracteres");
					input.nextLine();
					String texto = input.nextLine();
					if (texto.charAt(0)>='a'&&texto.charAt(0)<='z') {
						System.out.println("La primera letra es minúscula");
					}else {
						System.out.println("La primera letra no es minúscula");
					}
					if (texto.charAt(1)>='a'&&texto.charAt(1)<='z') {
						System.out.println("La segunda letra es minúscula");
					}else {
						System.out.println("La segunda letra no es minúscula");
					}
					break;
				case 4:
					//Opción 4 Esta opción solicita dos números y muestra los números pares entre ellos 
					System.out.println("Introduce el primer número");
					int n1=input.nextInt();
					System.out.println("Introduce el segundo número");
					int n2=input.nextInt();
					int max;
					int min;
					if (n1>n2) {
						max=n1;
						min=n2;
					}else {
						max=n2;
						min=n1;
					}
					for (int i=min; i<=max;i++) {
						if (i%2==0) {
							System.out.println(i);
						}
					}
					break;
				case 5:
					salir=true;
					break;
				default:
					System.out.println("Introduce un número entre 1 y 5");
			
			}
			
		}
		
		input.close();

	}

}
