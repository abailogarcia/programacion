package ejer3menu3opciones;

import java.util.Scanner;

public class Ejer3menu3opciones {

	public static void main(String[] args) {
		// Crear un programa que me muestre un menú y me pida seleccionar una opción. El
		// menú tendrá 3 opciones:
		// 1 – calcular importe
		// 2 – contraseña correcta
		// 3 - salir
		// Si selecciono otra opción no contemplada el programa me avisará. Debo usar un
		// switch para
		// ejecutar la opción pedida. El menú se repite después de cada opción (1 o 2)
		// hasta pulsar 3-salir.
		Scanner input = new Scanner(System.in);
		boolean salir = false;
		int opcion;

		while (!salir) {
			System.out.println("1 – calcular importe");
			System.out.println("2 – contraseña correcta");
			System.out.println("3 - salir ");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				// El programa pedirá introducir una cantidad por teclado correspondiente al
				// total
				// de una compra y un día de la semana. Si el día es martes o jueves, se
				// realizará un descuento del 15%
				// por la compra. Visualizar el descuento y el total a pagar por la compra.
				System.out.println("Introduzca el importe");
				double cantidad = input.nextDouble();
				double descuento = 0;
				System.out.println("Introduzca un día de la semana");
				input.nextLine();
				String dia = input.nextLine().toLowerCase();
				if (dia.equals("martes") || dia.equals("jueves")) {
					descuento = cantidad * 0.15;
					cantidad = cantidad * 0.85;

				}
				System.out.println("El importe a pagar es " + cantidad);
				System.out.println("El descuento aplicado es " + descuento);
				break;
			case 2:
				// El programa me pide una contraseña y muestra un único mensaje indicando
				// solamente si es válida o no. Para que sea válida debe contener al menos una
				// vocal, y solo puede
				// contener letras. Ni cifras, ni otros caracteres. (facilita las cosas,
				// convertir la cadena a mayúsculas o
				// minúsculas, con algún método String)
				input.nextLine();
				System.out.println("Introduce una contraseña sólo letras y con una vocal ");
				String pass = input.nextLine().toLowerCase();
				int longitud = pass.length();
				int cuentaVocales = 0;
				boolean correcto = true;	
				for (int i = 0; i < longitud; i++) {
					int car = (int) pass.charAt(i);
					// System.out.println(car);
					if (car < 97 || car > 122) {
						System.out.println("Sólo puede contener letras y vocales");
						correcto = false;
						break;
					} else if (car == 97 || car == 101 || car == 105 || car == 111 || car == 117) {
						cuentaVocales++;

					}
				}
				if (correcto && cuentaVocales>0) {
					System.out.println("El password es correcto");
				} else {
					System.out.println("El password es incorrecto");
				}
				break;
			case 3:
				salir = true;
				break;
			default:
				System.out.println("Introduce un valor entre 1 y 3");
			}

		}
		System.out.println("Se acabó");

		input.close();
	}

}
