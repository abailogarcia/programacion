package ejer2dibujarcuadrado;

import java.util.Scanner;

public class Ejer2dibujarcuadrado {

	public static void main(String[] args) {
		// Pedir un número entero que representa el tamaño del lado. Dibujar y rellenar un 
		//cuadrado con el carácter ‘X’, con tantas filas y columnas como se ha indicado por teclado.
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el tamaño del cuadrado");
		int lado = input.nextInt();
		
		for (int i =1; i<=lado;i++) {
			for (int j=1; j<= lado; j++) {
				System.out.print("X ");
				
			}
			System.out.println("");
		}
		
		input.close();
	}

}
