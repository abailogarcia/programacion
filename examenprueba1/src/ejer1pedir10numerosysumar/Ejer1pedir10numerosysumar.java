package ejer1pedir10numerosysumar;

import java.util.Scanner;

public class Ejer1pedir10numerosysumar {

	public static void main(String[] args) {
		// Pedir 10 números reales por teclado y después escribir la suma total. 
		
		Scanner input =new Scanner(System.in);
		double suma = 0;
		for (int i =1; i<=10; i++) {
			System.out.println("Introduce un número");
			double numero = input.nextDouble();
			suma += numero;
			
		}
		System.out.println("Su suma es "+suma);
		input.close();

	}

}
