package ejer4pedirnumerosysumedia;

import java.util.Scanner;

public class Ejer4pedirnumerosysumedia {

	public static void main(String[] args) {
		// Crear un programa que me pide un número positivo (veces). Después me pide y
		// lee
		// tantos números como me indica ese número inicial (veces). (1 pto).
		// Por cada número leído el programa me dice si es par o impar. (1 pto)
		// Al terminar de leer todos los números (cuando para el bucle) me calcula la
		// media de todos ellos. (1 pto)

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el número de veces");
		int veces = input.nextInt();
		int suma = 0;
		double numero = 0;
		for (int i = 1; i <= veces; i++) {
			System.out.println("Introduzca el número " + i);
			numero = input.nextDouble();
			suma += numero;
			if (numero % 2 == 0) {
				System.out.println("El número " + numero + " es par");
			} else {
				System.out.println("El número " + numero + " es impar");
			}
		}
		System.out.println("La media de los números es " + ((double)suma/veces));
		input.close();

	}

}
