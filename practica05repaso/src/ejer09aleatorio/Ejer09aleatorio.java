package ejer09aleatorio;

public class Ejer09aleatorio {

	public static void main(String[] args) {
		// Generar un número entero aleatorio entre 0 y 100 ayudándome del método Math.random(). 
		//Posteriormente muestro por pantalla todos los números desde el número aleatorio hasta el 
		//-100, de 7 en 7.
		double numero = Math.random()*100;
		System.out.println(numero);
		for (int i = (int)numero; i>=-100;i-=7) {
			System.out.println(i);
		}
		
	}

}
