package ejer14entradasysalidas;

import java.util.Scanner;

public class Ejer14entradasysalidas {

	public static void main(String[] args) {
		// Programa  para  controlar  el  nivel  de  un  depósito  de  agua.  El  programa  inicialmente  nos 
		//pedirá el volumen total en litros del depósito. Posteriormente nos pide constantemente la 
		//cantidad de litros que metemos al depósito o que retiramos (números positivos echamos, 
		//negativos  retiramos).  Después  de  cada  cantidad  que  introducimos,  nos  debe  mostrar  la 
		//cantidad restante de litros que hace falta para llenarlo completamente. El programa deja de 
		//pedirnos cantidades cuando se ha llegado o superado el límite del depósito, indicándonoslo 
		//por pantalla. 
		Scanner input = new Scanner(System.in);
		System.out.println("Dime el volumen inicial de litros del depósito");
		int volumen = input.nextInt();
		int queda =0;
		
		while (queda<volumen) {
			System.out.println("Introduce el movimiento de litros");
			int movimiento=input.nextInt();
			queda = queda + movimiento;
			System.out.println("Te faltan "+ ( volumen-queda)+" litros");
		}
		
		input.close();
		

	}

}
