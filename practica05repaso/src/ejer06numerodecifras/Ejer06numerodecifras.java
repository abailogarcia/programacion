package ejer06numerodecifras;

import java.util.Scanner;

public class Ejer06numerodecifras {

	public static void main(String[] args) {
		// Mostrar  el  número  de  cifras  de  un  número  introducido  por  teclado.  Los  datos  deben  ser 
		//tratados  como  números  enteros,  no  cómo  String  (Se  debe  resolver  sin  los  métodos  de  la 
			//	clase String). 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un número");
		int numero = input.nextInt();
		int contador=1;
		
		while (numero>10) {
			numero=numero/10;
			contador++;
		}
		System.out.println("El número tiene "+ contador + " cifras");
		
		input.close();
		
		

	}

}
