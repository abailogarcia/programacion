package ejer15numeroshasta0;

import java.util.Scanner;

public class Ejer15numeroshasta0 {

	public static void main(String[] args) {
		//  Programa que pide números enteros positivos y negativos por teclado hasta que 
		//introducimos el número 0. Después deberá indicar el número total de números leídos y la 
		//suma total de sus valores. 
		Scanner input = new Scanner(System.in);
		int numero=1;
		int contador = 0;
		int suma =0;
		while(numero!=0) {
			System.out.println("Introduce un número");
			numero =input.nextInt();
			contador++;
			suma+=numero;
			
		}
		System.out.println("Has introducido "+contador+" números");
		System.out.println("La suma de los números introducidos es "+suma);
		
		input.close();
		
	}

}
