package ejer13potencia;

import java.util.Scanner;

public class Ejer13potencia {

	public static void main(String[] args) {
		// Programa  que  permite  calcular  la  potencia  de  un  número.  Pedirá  por  teclado  la  base  y  el 
		//exponente y mostrará el resultado de la potencia. El exponente puede ser mayor o igual que 
		//0. Sin usar la clase Math. 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la base");
		int base = input.nextInt();
		System.out.println("Introduce el exponente");
		int exponente = input.nextInt();
		int resultado =1;
		for (int i=1; i<=exponente;i++) {
			resultado = base*resultado;
		}
		System.out.println("El resultado es "+resultado);
		input.close();

	}

}
