package ejer07tablamultiplicar;

import java.util.Scanner;

public class Ejer07tablamultiplicar {

	public static void main(String[] args) {
		// Mostrar la tabla de multiplicar de un numero introducido por teclado del 1 al 10.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca un número");
		int numero = input.nextInt();
		
		for (int i=1; i<=10;i++) {
			System.out.println(numero +" x " +i+" = "+(numero*i));
		}
		
		input.close();
	}

}
