package ejerciciosprevios;
//definición de la clase
//public class
public class Ejercicioprevio1 {
//definición del método main
//public static void main
	//public  accesible desde cualquier clase
	//static no requiere objetos (POO)
	//void lo que devuelve (void nada, int entero, double decimal...)
	//nombre del método suma()
	//notación métodos lowerCamelCase
	//suma()  sumaNumeros() sumaNumerosEnteros()
	//los paréntesis de suma() es lo que recibe -> en este caso nada
	

	public static void suma() {
		//suma dos números
		int numero1=3;
		int numero2=5;
		System.out.println("La suma es " +(numero1+numero2));
	}
	
	public static void multiplica() {
		//multiplica 2 números
		int numero1=4;
		int numero2=12;
		System.out.println("La multiplicación es "+(numero1*numero2));
	}
	public static void main(String[] args) {
		
		//llamo al método multiplicación
		multiplica();
		//llamo al método suma
		suma();
	}
}
