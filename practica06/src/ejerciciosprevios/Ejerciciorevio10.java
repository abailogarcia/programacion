package ejerciciosprevios;

import java.util.Scanner;

public class Ejerciciorevio10 {

	public static void main(String[] args) {
		System.out.println("Estoy en el main");
		// TODO Auto-generated method stub
		System.out.println("Llamo a longitudNombre()");
		longitudNombre();
		int longitudCadena=calculaLongitud();
		System.out.println(longitudCadena);
		//uso la variable que me ha dado el método calculaLongitud
		if (longitudCadena>5) {
			System.out.println("Es mayor de 5");
		}else {
			System.out.println("Es menor de 5");
		}

		//llamada a método que tenga un parámetro
		System.out.println("Llamo a cambiaMayusculas");
		String cadena="Esto es una frase en mayúsculas";
		cambiaMayusculas(cadena);
		
	}

	//declaramos los métodos y les damos funcionalidad
	//public no es necesario si lo voy a usar en la misma clase
	//static lo usamos para no trabajar con POO
	//llamamos directamente al método partiendo de la clase
	//Clase.metodo -> Ejercicioprevio10.longitudNombre()
	//si el método está en la misma clase no es necesario ponerlo
	//longitudNombre()
	//void cuando no devuelve nada, no sale nada del 'cajón' del método
	//nombre del método -> minúsculasMayusculas (lowerCamelCase)
	public static void longitudNombre() {
		Scanner input=new Scanner(System.in);
		System.out.println("Dame tu nombre");
		String nombre=input.nextLine();
		System.out.println("La longitud es " +nombre.length());		
		input.close();
	}
	//int me permite devolver un entero fuera del método
	public static int calculaLongitud() {
		String cadena="Hola caracola";
		int longitud =cadena.length();
		return longitud;
	}
	
	//método que recibe una cadena y la pasa a mayúsculas
	public static void cambiaMayusculas(String miCadena) {
		System.out.println(miCadena.toUpperCase());
	}
	
}
