package ejerciciosprevios;

public class Ejercicioprevio10 {

	public static void main(String[] args) {
		int numero1=10;
		int numero2=21;
		// llamamos a los métodos
		sumaNumeros(numero1, numero2);
		restaNumeros(numero1,numero2);
		double numero3=34;
		double numero4=43;
		multiplicaNumeros(numero3,numero4);
		divideNumeros(numero3,numero4);
		
	}

	//metodo suma void
	//lo que está entre paréntesis en un método se llama parámetro
	//son datos de entrada y no son los mismos datos que las variables
	public static void sumaNumeros(int num1, int num2) {
		int suma = num1+num2;
		System.out.println(suma);			
	}
	//metodo resta void
	public static void restaNumeros(int num1, int num2) {
		
		int resta=num1-num2;
		System.out.println(resta);
	}
	//método multiplica void
	public static void multiplicaNumeros(double num1, double num2) {
		
		double multiplica =num1*num2;
		System.out.println(multiplica);
	}
	
	//método divide void
	public static void divideNumeros(double num1, double num2) {
		
		double divide=(double)num1/(double)num2;
		System.out.println(divide);
	}
	
	
}
