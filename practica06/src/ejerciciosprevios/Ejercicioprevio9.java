package ejerciciosprevios;

public class Ejercicioprevio9 {

	public static void main(String[] args) {
		// llamamos a los métodos
		
		int sumado=sumaNumeros();
		System.out.println(sumado);
		int restado = restaNumeros();
		System.out.println(restado);
		double multiplicado=multiplicaNumeros();
		System.out.println(multiplicado);
		double division=divideNumeros();
		System.out.println(division);
		

	}

	//metodo suma int
	public static int sumaNumeros() {
		int n1=12;
		int n2=24;
		int suma = n1+n2;
		return suma;			
	}
	//metodo resta int
	public static int restaNumeros() {
		int n1=100;
		int n2=47;
		int resta=n1-n2;
		return resta;
	}
	//método multiplica double
	public static double multiplicaNumeros() {
		int n1=13;
		int n2=25;
		double multiplica =(n1*n2);
		return multiplica;
	}
	
	//método divide double
	public static double divideNumeros() {
		int n1=60;
		int n2=13;
		double divide=(double)n1/(double)n2;
		return divide;
	}
	
	
}
