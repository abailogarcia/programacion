package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio5 {

	//llamamos a métodos no estáticos
	//sólo los usados hasta ahora
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		System.out.println("Dame un número");
		
		//llamamos al método nextInt() usando input (objeto de la clase Scanner)
		int numero = input.nextInt();
		
		System.out.println(numero);
		
		
		input.close();

	}

}
