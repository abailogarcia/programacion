package ejerciciosprevios;

public class Ejercicioprevio8 {

	public static void main(String[] args) {
		// llamamos a los métodos
		
		sumaNumeros();
		restaNumeros();
		multiplicaNumeros();
		divideNumeros();
		

	}

	//metodo suma void
	public static void sumaNumeros() {
		int n1=12;
		int n2=24;
		System.out.println(n1+n2);			
	}
	//metodo resta void
	public static void restaNumeros() {
		int n1=100;
		int n2=47;
		System.out.println(n1-n2);
	}
	//método multiplica void
	public static void multiplicaNumeros() {
		int n1=13;
		int n2=25;
		System.out.println(n1*n2);
	}
	
	//método divide void
	public static void divideNumeros() {
		int n1=60;
		int n2=13;
		System.out.println((double)n1/(double)n2);
	}
	
	
}
