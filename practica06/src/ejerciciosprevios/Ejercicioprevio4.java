package ejerciciosprevios;

public class Ejercicioprevio4 {

	//ejercicio que llama a métodos ya construidos de java
	//clase.método
	public static void main(String[] args) {
		// llamo al método out.println de la clase System. método static
		System.out.println("hola");
		//llamo al método sqrt de la clase Math
		System.out.println(Math.sqrt(2.55));
		//llamo al método round de la clase Math
		System.out.println(Math.round(2.55));

	}

}
