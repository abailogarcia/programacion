package ejerciciosprevios;
import java.util.Scanner;
public class Ejercicioprevio18 {
static Scanner input=new Scanner(System.in);
	public static void main(String[] args) {
		// pido 2 números con un métood
		// hago operación suma en un método
		// muestro datos con un método
		double numerico1= numero();
		double numerico2= numero();
		double sumica=suma(numerico1, numerico2);
		resultado(sumica);
		
		input.close();
		
	}
	
	//pido datos
	public static double numero() {
		System.out.println("Introduce un número");
		double num=input.nextDouble();
		return num;
	}
	
	//hago operaciones
	public static double suma(double num1, double num2) {
		double suma=num1+num2;
		return suma;
	}
	
	//muestro datos
	public static void resultado(double sum) {
		System.out.println("El resultado es " + sum);
	}
}
