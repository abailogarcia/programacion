package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio16 {
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		// uso de un solo scanner
		//suma
		int numero1 = leerEntero();
		int numero2 = leerEntero();
		System.out.println("Suma números es "+ sumaEnteros(numero1, numero2));
		
		//resta
		System.out.println("Resta números es " + restaEnteros(numero1, numero2));
		
		//multiplica
		double numero3 = leerDoble();
		double numero4 = leerDoble();
		System.out.println("Multiplica números es "+ multiplicaNumeros(numero3, numero4));
		
		//divide
		System.out.println("Multiplica números es "+ divideNumeros(numero3, numero4));
		
		input.close();
	}
	//método para pedir los datos enteros
	public static int leerEntero() {
		System.out.println("Introduce un entero");
		int numero = input.nextInt();
		return numero;
	}
	
	//método para pedir los datos doubles
	public static int leerDoble() {
		System.out.println("Introduce un doble");
		int doble = input.nextInt();
		return doble;
	}
	//método que recibe 2 enteros y devuelve un entero que es la suma
	public static int sumaEnteros(int num1, int num2) {
		int suma = (num1+num2);
		return suma;
	}
	
	//método que recibe 2 enteros y devuelve un entero que es la resta
	public static int restaEnteros(int num1, int num2) {
		int resta = (num1-num2);
		return resta;
	}
	//método que recibe 2 doubles y devuelve un double que es la multiplicación
	public static double multiplicaNumeros(double num1, double num2) {
		double multiplica = (num1*num2);
		return multiplica;
	}
	//método que recibe 2 doubles y devuelve un double que es la división
		public static double divideNumeros(double num1, double num2) {
			double divide = (num1/num2);
			return divide;
		}

}
