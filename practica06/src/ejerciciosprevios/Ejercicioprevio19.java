package ejerciciosprevios;

public class Ejercicioprevio19 {

	public static void main(String[] args) {
		// Menú navidad
		//llamamos al título
		menu();
				
		//llamamos al primero
		primero();
		//llamamos al segundo
		segundo();
		//llamamos al tercero
		postre();
		//Mostramos todo el menú junto
	}

	
	//método menú que muestra cabecera menú navidad
	static void menu() {
		System.out.println("Este es el menú del restaurante Montessori, bueno y barato");
	}
	
	//método que muestre el contenido del primero
	static void primero() {
		System.out.println("Ensalada aragonesa");
	}
	
	//método que muestre el contenido del segundo
	static void segundo() {
		System.out.println("Ternasco asado con pataticas panadera");
	}
	
	//método que muestre el contenido del postre
	static void postre() {
		System.out.println("Espuma de tiramisú");
	}
	
	
}
