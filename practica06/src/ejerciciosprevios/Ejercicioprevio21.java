package ejerciciosprevios;

public class Ejercicioprevio21 {

	public static void main(String[] args) {
		// factorial de 5=5*4*3*2*1
		System.out.println("Factorial de 6 " +factorial(6));
		System.out.println("Factorial de 5 " +factorial(5));
		System.out.println("Factorial de 4 " +factorial(4));
		System.out.println("Factorial de 3 " +factorial(3));
		System.out.println("Factorial de 2 " +factorial(2));
		System.out.println("Factorial de 1 " +factorial(1));
		System.out.println("Factorial de 0 " +factorial(0));
		

	}
	//método recursivo -> recursividad
	//usar un método para llamarse a sí mismo
	static int factorial(int entero) {
		if(entero==0) {
			return 1;
		}
		return factorial(entero-1)*entero;
	}
	

}
