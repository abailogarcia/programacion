package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio12 {

	public static void main(String[] args) {
		// pido un cadena
		//llamo a método void cambiarMayusculas(cadena)
		//llamo a método void cambiarMinusculas(cadena)
		//llamo a método void longitud(cadena)
		//llamo a método void primeraLetra(cadena)
		Scanner input=new Scanner(System.in);
		System.out.println("Dame un texto");
		String miTexto=input.nextLine();
		cambiarMayusculas(miTexto);
		cambiarMinusculas(miTexto);
		longitud(miTexto);
		primeraLetra(miTexto);
		input.close();
	}

	//método cambiarMayusculas(cadena)
	public static void cambiarMayusculas(String texto) {
		System.out.println(texto.toUpperCase());
	}
	
	//método cambiarMinusculas(cadena)
	public static void cambiarMinusculas(String texto) {
		System.out.println(texto.toLowerCase());
	}
	
	//método longitud(cadena)
	public static void longitud(String texto) {
		System.out.println(texto.length());
	}
	
	//método primeraLetra(cadena)
	public static void primeraLetra(String texto) {
		System.out.println(texto.charAt(0));
	}
}
