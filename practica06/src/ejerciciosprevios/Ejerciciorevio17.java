package ejerciciosprevios;

public class Ejerciciorevio17 {

	public static void main(String[] args) {
		// Sobrecarga -> puede haber varios métodos que se llaman igual pero deben ser diferentes en el número
		//de parámetros o el tipo de parámetros
		//no puede ser diferente sólo en el valor de devolución
		suma(2,3);
		suma(2,3,4);
		suma (2,3.5);
		// no funciona suma (2,3.5, 3);
	}
	//método suma que reicibe dos enteros
	static void suma(int num1, int num2) {
		System.out.println("Estoy en suma con 2 enteros");
		System.out.println(num1+num2);
		
	}
	//método suma que recibe 2 dobles
	static void suma(double num1, double num2) {
		System.out.println("Estoy en suma con 2 dobles");
		System.out.println(num1+num2);
		
	}
	//método suma que recibe tres enteros
	static void suma(int num1, int num2, int num3) {
		System.out.println("Estoy en suma con 3 enteros");
		System.out.println(num1+num2+num3);
	
	}
	//método suma que recibe 1 erntero y un doble
		static void suma(int num1, double num2) {
			System.out.println("Estoy en suma con 1 entero y un doble");
			System.out.println(num1+num2);
		}
	
}
