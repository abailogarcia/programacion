package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio11 {

	public static void main(String[] args) {
		// llamadas a métodos
		//pedir 1 número por teclado
		//llamar al método void multiploDos(numero)
		//llamar al método void multiploTres(numero)
		//llamar al método void multiploCuatro(numero)
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un número");
		int numero=input.nextInt();
		multiploDos(numero);
		multiploTres(numero);
		multiploCuatro(numero);
		
		input.close();
	}

	// método void multiploDos(numero)
	public static void multiploDos(int numerico) {
		if(numerico%2==0) {
			System.out.println("El número es múltiplo de 2");
		} else {
			System.out.println("El número no es múltiplo de 2");
		}
	}
	
	// método void multiploTres(numero)
	public static void multiploTres(int numerico) {
		if(numerico%3==0) {
			System.out.println("El número es múltiplo de 3");
		} else {
			System.out.println("El número no es múltiplo de 3");
		}
	}
	
	// método void multiploCuatro(numero)
	public static void multiploCuatro(int numerico) {
		if(numerico%4==0) {
			System.out.println("El número es múltiplo de 4");
		} else {
			System.out.println("El número no es múltiplo de 4");
		}
	}
	
}
