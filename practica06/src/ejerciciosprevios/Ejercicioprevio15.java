package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio15 {

	public static void main(String[] args) {
		// pido un cadena
		//llamo a método void cambiarMayusculas(cadena)
		//llamo a método void cambiarMinusculas(cadena)
		//llamo a método void longitud(cadena)
		//llamo a método void primeraLetra(cadena)
		Scanner input=new Scanner(System.in);
		System.out.println("Dame un texto");
		String miTexto=input.nextLine();
		menu();
		int opcion = input.nextInt();
				
		switch (opcion) {
			case 1:
				System.out.print("El texto en mayúsculas es ");
				cambiarMayusculas(miTexto);
				break;
			case 2:
				System.out.print("El texto en minúsculas es ");
				cambiarMinusculas(miTexto);
				break;
			case 3:
				System.out.print("La longitud del texto es ");
				longitud(miTexto);
				break;
			case 4:
				System.out.print("La primera letra es ");
				primeraLetra(miTexto);
				break;
			case 5:
				System.out.println("Se acabó");
				break;
			default:
				System.out.println("No es una buena opción");
				break;
		}
				
		input.close();
	}

	//método con el menú
	public static void menu() {
		
		System.out.println("Introduce una opción");
		System.out.println("1.- Mayúsculas");
		System.out.println("2.- Minúsculas");
		System.out.println("3.- Longitud del texto");
		System.out.println("4.- Primera letra");
		System.out.println("5.- Salir");
		
	}
	
	//método cambiarMayusculas(cadena)
	public static void cambiarMayusculas(String texto) {
		System.out.println(texto.toUpperCase());
	}
	
	//método cambiarMinusculas(cadena)
	public static void cambiarMinusculas(String texto) {
		System.out.println(texto.toLowerCase());
	}
	
	//método longitud(cadena)
	public static void longitud(String texto) {
		System.out.println(texto.length());
	}
	
	//método primeraLetra(cadena)
	public static void primeraLetra(String texto) {
		System.out.println(texto.charAt(0));
	}
}
