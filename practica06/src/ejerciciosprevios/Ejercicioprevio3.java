package ejerciciosprevios;
//definición de la clase
//public class
public class Ejercicioprevio3 {
//definición del método main
//public static void main
	public static void main(String[] args) {
		
		//llamo al método multiplicación
		multiplica();
		//llamo al método suma
		suma();
	}

	public static void suma() {
		//suma dos números
		int numero1=3;
		int numero2=5;
		System.out.println("La suma es " +(numero1+numero2));
	}
	
	public static void multiplica() {
		//multiplica 2 números
		int numero1=4;
		int numero2=12;
		System.out.println("La multiplicación es "+(numero1*numero2));
	}
	
}
