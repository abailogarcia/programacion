package ejerciciosprevios;

import java.util.Scanner;

public class Ejercicioprevio20menunavidad {
	static Scanner input=new Scanner(System.in);
	public static void main(String[] args) {
		// Menú navidad
		//llamamos al título
		menu();
				
		//llamamos al primero
		String primer=primero();
		
		//System.out.println(primero());
		
		//llamamos al segundo
		String segun=segundo();
		//llamamos al tercero
		String lamineria=postre();
		//Mostramos todo el menú junto
		System.out.println("Su menú es ");
		System.out.println(primer);
		System.out.println(segun);
		System.out.println(lamineria);
		
		input.close();
	}

	
	//método menú que muestra cabecera menú navidad
	static void menu() {
		System.out.println("Este es el menú del restaurante Montessori, bueno y barato");
	}
	
	//método que muestre el contenido del primero
	static String primero() {
		System.out.println("Selecciona un primer plato");
		System.out.println("1.-Ensalada aragonesa");
		System.out.println("2.-Gulas con almejas");
		System.out.println("3.-Mariscada");
		int opcion=input.nextInt();
		String primerPlato="";
		switch (opcion) {
			case 1:
				primerPlato="Ensalada aragonesa";
				break;
			case 2:
				primerPlato="Gulas con almejas";
				break;
			case 3:
				primerPlato="Mariscada";
				break;
			default:
				primerPlato="¿Estás a régimen?";
				break;
		}
		return primerPlato;
		
	}
	
	//método que muestre el contenido del segundo
	static String segundo() {
		System.out.println("Selecciona un segundo plato");
		System.out.println("1.-Ternasco asado con pataticas panadera");
		System.out.println("2.-Pulpo brasa con puré de cachelos");
		System.out.println("3.-Pularda rellena de foie");
		int opcion=input.nextInt();
		String segundoPlato="";
		switch (opcion) {
			case 1:
				segundoPlato="Ternasco asado con pataticas panadera";
				break;
			case 2:
				segundoPlato="Pulpo brasa con puré de cachelos";
				break;
			case 3:
				segundoPlato="Pularda rellena de foie";
				break;
			default:
				segundoPlato="¿Estás a régimen?";
				break;
		}
		return segundoPlato;
	}
	
	//método que muestre el contenido del postre
	static String postre() {
		System.out.println("Selecciona un postre");
		System.out.println("1.-Espuma de tiramisú");
		System.out.println("2.-Natillas de la abuela con galleta");
		System.out.println("3.-Surtido de chocolates");
		int opcion=input.nextInt();
		String postrePlato="";
		switch (opcion) {
			case 1:
				postrePlato="Espuma de tiramisú";
				break;
			case 2:
				postrePlato="Natillas de la abuela con galleta";
				break;
			case 3:
				postrePlato="Surtido de chocolates";
				break;
			default:
				postrePlato="¿Estás a régimen?";
				break;
		}
		return postrePlato;
	}
	
	
}
