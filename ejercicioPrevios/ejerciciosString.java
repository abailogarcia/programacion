package ejercicioPrevios;

public class ejerciciosString {

	public static void main(String[] args) {

		// Las raices cuadradas se hacen con Math.sqrt
		System.out.println(Math.sqrt(4));

		String cadena1 = "Hola";
		String cadena2 = "Caracola";
		
		// replaceAll -> replaza el caracter por lo que escoges
		String cadena3 = cadena1.replaceAll("a", "aaaaa");
		System.out.println("cadena1 " + cadena1);
		System.out.println("cadena2 " + cadena2);
		System.out.println("cadena3 " + cadena3);

		// equals -> mira si los valores que has escogido son iguales
		String cadena4 = "Hola";
		System.out.println("iguales " + cadena1.equals(cadena2));
		System.out.println("iguales " + cadena1.equals(cadena4));

		// compareTo -> compara alfabeticamente los valores que has escogido
		System.out.println(cadena1.compareTo(cadena2));

		// charAt -> muesta la letra que escogas (0 = 1� letra)
		System.out.println("Primera letra de cadena -> " + cadena1.charAt(0));

		// subString -> muestra las parabras que escogas ( entre los numeros que pongas)
		System.out.println("2 primeras letras de la cadena -> " + cadena1.substring(0, 3));

		// contains -> comprube a si tiene alguna palabra en comun entre las variables
		String cadena5 = "Hola caracola";
		System.out.println(cadena5.contains(cadena4));
		System.out.println(cadena1.contains(cadena2));

		// indexOf -> te dice la posicion de la letra (Empezando por 0) (Distinge entre mayusculas y minisculas)
		System.out.println(cadena5.indexOf('H'));
		// devuelve solo la primera letra encontrada
		System.out.println(cadena5.indexOf('o'));

		// startsWith -> compueba si comienza por el valor puesto
		System.out.println(cadena5.startsWith("H"));
		System.out.println(cadena5.startsWith("ola"));

		// endsWinth -> comprueba si finaliza por el valor puesto
		System.out.println(cadena5.endsWith("ola"));
		System.out.println(cadena5.endsWith("tora"));
		
	}
}
